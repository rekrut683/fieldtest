﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FieldTest
{
    class LossTimeInfo
    {
        public int _tick;
        public DateTime _time;
        public double _estSeconds;
    }


    class Statistics
    {
        Form1 listener;
        public Int32 sumPackets { get; set; }
        public Int32 intervalDuty { get; set; }
        public Int32 receivedPackets { get; set; }
        public Int32 brokenPackets { get; set; }
        public int percentDelivery { get; set; }
        public int rssiUseful { get; set; }
        public Int64 firstId { get; set; }
        public string lossIds { get; set; }
        public List<long> lossIdsArray { get; set; }
        public string statusFinish { get; set; }
        public List<LossTimeInfo> infoLossData = new List<LossTimeInfo>();


        public Statistics(Form1 f)
        {
            listener = f;
            lossIdsArray = new List<long>();
            Clear();
        }

        public void Fill(DateTime _begin, DateTime _end)
        {
            listener.UpdateLabel(listener.lblReceived, receivedPackets.ToString());
            listener.UpdateLabel(listener.lblBroken, brokenPackets.ToString());
            listener.UpdateLabel(listener.lblPercent, percentDelivery.ToString() + "%");
            listener.UpdateLabel(listener.lblRssi, rssiUseful.ToString());
            listener.UpdateLabel(listener.lblFirstId, firstId.ToString());

            StringBuilder sb = new StringBuilder();
            foreach(var i in lossIdsArray)
            {
                sb.Append(i.ToString());
                sb.Append(",");
            }
            lossIds = sb.ToString();

            listener.UpdateTextBox(listener.tboxLossIds, lossIds);
            //SetLossIdsArray(lossIds);
            listener.UpdateLabel(listener.lblLossCount, lossIdsArray.Count.ToString());
            listener.UpdateLabel(listener.lblStatusFinish, statusFinish);

            /*
            GraphController gController = GraphController.PacketsGraph;
            GraphAbs graph = gController.GetGraph(GraphController.TYPE_GRAPH.TIME);
            graph.Fill(sumPackets, (double)intervalDuty / 1000.0, lossIdsArray);
            gController.Draw(graph);
            */

            Graphs.Graph graph = Graphs.GraphController.Graphics[Graphs.GraphController.GRAPH_TYPE.HOLE_TIME];
            graph.Fill(new Graphs.GraphParams()
            {
                countPoints = sumPackets,
                xInterval = (double)intervalDuty / 1000.0,
                yInterval = 1.0,
                needPointsX = lossIdsArray
            });
            graph.DrawWithRescale();
        }

        public void Clear()
        {
            receivedPackets = 0;
            brokenPackets = 0;
            percentDelivery = 0;
            rssiUseful = 0;
            firstId = 0;
            lossIds = "";
            statusFinish = "";
            lossIdsArray.Clear();

            listener.UpdateLabel(listener.lblReceived, "-");
            listener.UpdateLabel(listener.lblBroken, "-");
            listener.UpdateLabel(listener.lblPercent, "-");
            listener.UpdateLabel(listener.lblRssi, "-");
            listener.UpdateLabel(listener.lblFirstId, "-");
            listener.UpdateTextBox(listener.tboxLossIds, "");
            listener.UpdateLabel(listener.lblLossCount, "-");
            listener.UpdateLabel(listener.lblStatusFinish, "");

            Graphs.GraphController.Graphics[Graphs.GraphController.GRAPH_TYPE.HOLE_TIME].Clear();
            Graphs.GraphController.Graphics[Graphs.GraphController.GRAPH_TYPE.HOLE_TIME].Clear();

            //GraphController.PacketsGraph.ClearView();
        }

        private void SetLossIdsArray(string ids)
        {
            string[] arr = ids.Split(',');
            foreach(var s in arr)
            {
                if(!string.IsNullOrEmpty(s))
                {
                    lossIdsArray.Add(Convert.ToInt64(s));
                }
            }
        }
    }
}
