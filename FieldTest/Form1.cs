﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using Newtonsoft.Json;

namespace FieldTest
{
    delegate void UpdateLabelDelegate(Label lbl, string msg);
    delegate void UpdateTextBoxDelegate(TextBox tbox, string msg);
    delegate void UpdateProgressDelegate(ProgressBar pbar, int value);


    delegate void UpdateChartDelegate(Chart ch, double[,] data);
    delegate void UpdateChartMinValueDelegate(Chart ch, double value);
    delegate void UpdateChartMaxValueDelegate(Chart ch, double value);
    delegate void UpdateChartGridDelegate(Chart ch, double value);
    delegate void ClearChartDelegate(Chart ch);
    public partial class Form1 : Form
    {

        Driver driver;

        //Point _mousePositionChart;

        //GraphWrapper graphWrapper;
        //GraphController graphController;
        

        public Form1()
        {
            InitializeComponent();
            cmbSerial.Items.AddRange(Driver.GetSerialPorts());
            lblReceived.Text = "-";
            lblPercent.Text = "-";
            lblFirstId.Text = "-";
            lblBroken.Text = "-";
            lblLossCount.Text = "-";
            lblRssi.Text = "-";
            lblTimeBegin.Text = "-";
            lblTimeFinish.Text = "-";
            lblTmeEstimatedSeconds.Text = "-";
            lblTimeEstimatedMinute.Text = "-";
            
            driver = new Driver(this);
            chart1.MouseWheel += chartZoom;
            chart1.MouseEnter += chartTracking_MouseEnter;
            chart1.MouseLeave += chartTracking_MouseLeave;
            chart1.MouseMove += chartDrag;
            chart1.MouseDown += lb_MouseDown;
            this.MouseDown += Form_MouseDown;
            this.KeyDown += Form1_KeyDown;

            // добавляем 2 полотна
            Graphs.GraphWrapper wrapperHole = new Graphs.GraphWrapper(chart1);
            Graphs.GraphWrapper wrapperRssi = new Graphs.GraphWrapper(chart2);
            Graphs.GraphController.GraphWrappers.Add(Graphs.GraphController.CHART_TYPE.HOLE, wrapperHole);
            Graphs.GraphController.GraphWrappers.Add(Graphs.GraphController.CHART_TYPE.RSSI, wrapperRssi);

            // добавляем всевозможные ряды для графиков
            Graphs.SeriesWrapper seriesHole = new Graphs.SeriesWrapper(wrapperHole, "Hole", SeriesChartType.StepLine, System.Drawing.Color.Blue, 3);
            Graphs.SeriesWrapper seriesRssiGood = new Graphs.SeriesWrapper(wrapperRssi, "FullRssi", SeriesChartType.Point, System.Drawing.Color.Blue, 2);
            Graphs.SeriesWrapper seriesRssiBad = new Graphs.SeriesWrapper(wrapperRssi, "Bad", SeriesChartType.Point, System.Drawing.Color.Red, 5);

            // добавляем графики
            Graphs.Graph graphHoleTime = new Graphs.Graph(seriesHole);
            Graphs.Graph graphHoleDistance = new Graphs.Graph(seriesHole);
            Graphs.Graph graphHoleTimeInverse = new Graphs.Graph(seriesHole);
            Graphs.Graph graphHoleDistanceInverse = new Graphs.Graph(seriesHole);
            Graphs.Graph graphRssiGood = new Graphs.Graph(seriesRssiGood);
            Graphs.Graph graphRssiBad = new Graphs.Graph(seriesRssiBad);

            Graphs.GraphController.Graphics.Add(Graphs.GraphController.GRAPH_TYPE.HOLE_DISTANCE, graphHoleDistance);
            Graphs.GraphController.Graphics.Add(Graphs.GraphController.GRAPH_TYPE.HOLE_DISTANCE_INVERSE, graphHoleDistanceInverse);
            Graphs.GraphController.Graphics.Add(Graphs.GraphController.GRAPH_TYPE.HOLE_TIME, graphHoleTime);
            Graphs.GraphController.Graphics.Add(Graphs.GraphController.GRAPH_TYPE.HOLE_TIME_INVERSE, graphHoleTimeInverse);
            Graphs.GraphController.Graphics.Add(Graphs.GraphController.GRAPH_TYPE.RSSI_GOOD, graphRssiGood);
            Graphs.GraphController.Graphics.Add(Graphs.GraphController.GRAPH_TYPE.RSSI_BAD, graphRssiBad);
        }



        public void UpdateProgress(ProgressBar pbar, int value)
        {
            if (pbar.InvokeRequired)
            {
                pbar.Invoke(new UpdateProgressDelegate(UpdateProgress), new object[] { pbar, value });
                return;
            }
            if(value > pbar.Maximum)
            {
                value = pbar.Maximum;
            }
            pbar.Value = value;
        }

        public void UpdateLabel(Label lbl, string msg)
        {
            if(lbl.InvokeRequired)
            {
                lbl.Invoke(new UpdateLabelDelegate(UpdateLabel), new object[] { lbl, msg });
                return;
            }
            lbl.Text = msg;
        }

        public void UpdateTextBox(TextBox tbox, string msg)
        {
            if (tbox.InvokeRequired)
            {
                tbox.Invoke(new UpdateTextBoxDelegate(UpdateTextBox), new object[] { tbox, msg });
                return;
            }
            tbox.Text = msg;
        }

        public void AddToTextBox(TextBox tbox, string msg)
        {
            if (tbox.InvokeRequired)
            {
                tbox.Invoke(new UpdateTextBoxDelegate(AddToTextBox), new object[] { tbox, msg });
                return;
            }
            string ss = tbox.Text;
            tbox.Text = ss + msg;
        }

        private void BtnStart_Click(object sender, EventArgs e)
        {
            object settings = new Driver.TestMode
            {
                _channel = Convert.ToInt32(tboxChannel.Text),
                _countPackets = Convert.ToUInt64(tboxCntPackets.Text),
                _interval = Convert.ToUInt64(tboxInterval.Text)
            };
            pBarPackets.Maximum = Convert.ToInt32(tboxCntPackets.Text);
            driver.Start(cmbSerial.Text, settings);
            //driver = new Driver(this, cmbSerial.Text, settings);
        }

        private void BtnRefreshSerial_Click(object sender, EventArgs e)
        {
            cmbSerial.Items.Clear();
            cmbSerial.Items.AddRange(Driver.GetSerialPorts());
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            //if (driver != null)
            //{
            //    driver.Stop();
            //}
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void Label5_Click(object sender, EventArgs e)
        {

        }

        private void BtnStop_Click(object sender, EventArgs e)
        {
            if (driver != null)
            {

                //driver = null;
            }
        }



        private void BtnTime_Click(object sender, EventArgs e)
        {
            /*if(GraphController.PacketsGraph.GetGraph(GraphController.TYPE_GRAPH.TIME).IsInited)
                GraphController.PacketsGraph.Draw(GraphController.PacketsGraph.GetGraph(GraphController.TYPE_GRAPH.TIME));
                */

            Graphs.GraphController.Graphics[Graphs.GraphController.GRAPH_TYPE.HOLE_TIME].DrawWithRescale();
        }

        private void BtnDistance_Click(object sender, EventArgs e)
        {
            double speed = 0.0;
            try
            {
                speed = Convert.ToDouble(tboxSpeed.Text);
            }
            catch (Exception ex)
            {
                UpdateTextBox(tboxSerial, "error value");
                return;
            }
            speed = (double)speed / 3.6;

            Graphs.GraphController.Graphics[Graphs.GraphController.GRAPH_TYPE.HOLE_DISTANCE] = (Graphs.Graph)Graphs.GraphController.Graphics[Graphs.GraphController.GRAPH_TYPE.HOLE_TIME].Clone();
            Graphs.GraphController.Graphics[Graphs.GraphController.GRAPH_TYPE.HOLE_DISTANCE].RescaleX(speed);
            Graphs.GraphController.Graphics[Graphs.GraphController.GRAPH_TYPE.HOLE_DISTANCE].DrawWithRescale();

            /*
            GraphAbs graph = GraphController.PacketsGraph.GetGraph(GraphController.TYPE_GRAPH.DISTANCE);
            graph.Fill(GraphController.PacketsGraph.GetGraph(GraphController.TYPE_GRAPH.TIME), speed);
            GraphController.PacketsGraph.Draw(graph);
            */
            
        }


        private void chartTracking_MouseEnter(object sender, EventArgs e)
        {
            this.chart1.Focus();
        }

        private void chartTracking_MouseLeave(object sender, EventArgs e)
        {
            this.chart1.Parent.Focus();
        }

        void lb_MouseDown(object sender, MouseEventArgs e)
        {
            //if (e.Button == MouseButtons.Left)
             //   _mousePositionChart = e.Location;
        }

        private void chartDrag(object sender, MouseEventArgs e)
        {
            /*
            if (dataChart == null && dataChartDistance == null) return;

            if (e.Button == MouseButtons.Left)
            {
                // влево
                if(e.Location.X - _mousePositionChart.X > 0)
                {
                    int a = 0;
                    double val = Math.Round(chart1.ChartAreas[0].AxisX.Minimum - 0.05, 2);
                    if (val < 0) val = 0;
                    chart1.ChartAreas[0].AxisX.Minimum = val;

                    //if (dataChart == null) return;
                    val = Math.Round(chart1.ChartAreas[0].AxisX.Maximum - 0.05, 2);
                    if (val < 0.5) val = 0.5;
                    chart1.ChartAreas[0].AxisX.Maximum = val;
                    //UpdateChart(chart1, null);
                }



                // вправо
                if (e.Location.X - _mousePositionChart.X < 0)
                {
                    double val = Math.Round(chart1.ChartAreas[0].AxisX.Minimum + 0.05, 2);
                    //if (val < 0) val = 0;
                    chart1.ChartAreas[0].AxisX.Minimum = val;

                    //if (dataChart == null) return;
                    chart1.ChartAreas[0].AxisX.Maximum = Math.Round(chart1.ChartAreas[0].AxisX.Maximum + 0.05, 2);
                    //UpdateChart(chart1, null);
                    int b = 0;
                }


                /*if (Math.Abs(e.Location.X - _mousePositionChart.X) + Math.Abs(e.Location.Y - _mousePositionChart.Y) > 3)
                {
                    int i = 0;
                }*/
           // }
        }

        private void chartZoom(object sender, MouseEventArgs e)
        {
            /*
            if(e.Delta < 0)
            {
                GraphAbs graph = GraphController.PacketsGraph.GetActiveGraph();
                GraphController.PacketsGraph.ZoomInX(graph);
            }
            else if(e.Delta > 0)
            {
                GraphAbs graph = GraphController.PacketsGraph.GetActiveGraph();
                GraphController.PacketsGraph.ZoomOutX(graph);
            }
            */
        }

        private void Form_MouseDown(object sender, EventArgs e)
        {
            this.Focus();
        }

        private void Form1_KeyPress(object sender, KeyPressEventArgs e)
        {
            // zoom in
            /*if(e.KeyChar == (char)Keys.PageUp)
            {
                double val = Math.Round(chart1.ChartAreas[0].AxisX.Minimum + 0.1, 1);
                //if (val < 0) val = 0;
                chart1.ChartAreas[0].AxisX.Minimum = val;

                //if (dataChart == null) return;
                chart1.ChartAreas[0].AxisX.Maximum = Math.Round(chart1.ChartAreas[0].AxisX.Maximum - 0.1, 1);
                UpdateChart(chart1, null);
            }

            //zoom out
            if (e.KeyChar == (char)Keys.PageDown)
            {
                double val = Math.Round(chart1.ChartAreas[0].AxisX.Minimum - 0.1, 1);
                if (val < 0) val = 0;
                chart1.ChartAreas[0].AxisX.Minimum = val;

                //if (dataChart == null) return;
                chart1.ChartAreas[0].AxisX.Maximum = Math.Round(chart1.ChartAreas[0].AxisX.Maximum + 0.1, 1);
                UpdateChart(chart1, null);
            }

            //left shift
            if (e.KeyChar == (char)Keys.Home)
            {

            }

            //right shift
            if (e.KeyChar == (char)Keys.End)
            {

            }*/
        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            // zoom in
            if (e.KeyCode == Keys.PageUp)
            {
                double val = Math.Round(chart1.ChartAreas[0].AxisX.Minimum + 0.1, 1);
                //if (val < 0) val = 0;
                chart1.ChartAreas[0].AxisX.Minimum = val;

                //if (dataChart == null) return;
                chart1.ChartAreas[0].AxisX.Maximum = Math.Round(chart1.ChartAreas[0].AxisX.Maximum - 0.1, 1);
                //UpdateChart(chart1, null);
            }

            //zoom out
            if (e.KeyCode == Keys.PageDown)
            {
                double val = Math.Round(chart1.ChartAreas[0].AxisX.Minimum - 0.1, 1);
                if (val < 0) val = 0;
                chart1.ChartAreas[0].AxisX.Minimum = val;

                //if (dataChart == null) return;
                chart1.ChartAreas[0].AxisX.Maximum = Math.Round(chart1.ChartAreas[0].AxisX.Maximum + 0.1, 1);
                //UpdateChart(chart1, null);
            }

            //left shift
            if (e.KeyCode == Keys.Home)
            {
                int a = 0;
                double val = Math.Round(chart1.ChartAreas[0].AxisX.Minimum - 0.05, 2);
                if (val < 0) val = 0;
                chart1.ChartAreas[0].AxisX.Minimum = val;

                //if (dataChart == null) return;
                val = Math.Round(chart1.ChartAreas[0].AxisX.Maximum - 0.05, 2);
                if (val < 0.5) val = 0.5;
                chart1.ChartAreas[0].AxisX.Maximum = val;
                //UpdateChart(chart1, null);
            }

            //right shift
            if (e.KeyCode == Keys.End)
            {
                double val = Math.Round(chart1.ChartAreas[0].AxisX.Minimum + 0.05, 2);
                //if (val < 0) val = 0;
                chart1.ChartAreas[0].AxisX.Minimum = val;

                //if (dataChart == null) return;
                chart1.ChartAreas[0].AxisX.Maximum = Math.Round(chart1.ChartAreas[0].AxisX.Maximum + 0.05, 2);
                //UpdateChart(chart1, null);
                int b = 0;
            }
        }

        private void TboxLossIds_TextChanged(object sender, EventArgs e)
        {
            tboxLossIds.SelectionStart = tboxLossIds.Text.Length;
            tboxLossIds.ScrollToCaret();
            tboxLossIds.Refresh();
        }

        private void TboxSerial_TextChanged(object sender, EventArgs e)
        {
            tboxSerial.SelectionStart = tboxSerial.Text.Length;
            tboxSerial.ScrollToCaret();
            tboxSerial.Refresh();
        }

        private void BtnInverse_Click(object sender, EventArgs e)
        {
            Graphs.Graph curGraph = Graphs.GraphController.GetIsDrawGraph(Graphs.GraphController.GraphWrappers[Graphs.GraphController.CHART_TYPE.HOLE]);
            if (curGraph == null) return;
            if (curGraph == Graphs.GraphController.Graphics[Graphs.GraphController.GRAPH_TYPE.HOLE_DISTANCE_INVERSE] ||
                curGraph == Graphs.GraphController.Graphics[Graphs.GraphController.GRAPH_TYPE.HOLE_TIME_INVERSE])
            {
                return;
            }

            Graphs.Graph graph = (Graphs.Graph)curGraph.Clone();
            graph.InverseX();
            if (curGraph == Graphs.GraphController.Graphics[Graphs.GraphController.GRAPH_TYPE.HOLE_DISTANCE])
            {
                Graphs.GraphController.Graphics[Graphs.GraphController.GRAPH_TYPE.HOLE_DISTANCE_INVERSE] = graph;
            }
            else if (curGraph == Graphs.GraphController.Graphics[Graphs.GraphController.GRAPH_TYPE.HOLE_TIME])
            {
                Graphs.GraphController.Graphics[Graphs.GraphController.GRAPH_TYPE.HOLE_TIME_INVERSE] = graph;
            }
            graph.DrawWithRescale();
        }

        private void BtnSetMinimal_Click(object sender, EventArgs e)
        {
            string s = tboxMinValueChart.Text;
            s = s.Replace(".", ",");
            Graphs.Graph graph = Graphs.GraphController.GetIsDrawGraph(Graphs.GraphController.GraphWrappers[Graphs.GraphController.CHART_TYPE.HOLE]);
            if (graph == null) return;
            double dValue = 0.0;
            try
            {
                dValue = Convert.ToDouble(s);
            }
            catch(Exception ex)
            {
                return;
            }
            graph.SetMin(graph.AxisX, dValue);         
        }

        private void BtnRestore_Click(object sender, EventArgs e)
        {
            Graphs.Graph graph = Graphs.GraphController.GetIsDrawGraph(Graphs.GraphController.GraphWrappers[Graphs.GraphController.CHART_TYPE.HOLE]);
            if (graph == null) return;
            if (graph == Graphs.GraphController.Graphics[Graphs.GraphController.GRAPH_TYPE.HOLE_TIME_INVERSE])
            {
                graph = Graphs.GraphController.Graphics[Graphs.GraphController.GRAPH_TYPE.HOLE_TIME];
            }
            else if (graph == Graphs.GraphController.Graphics[Graphs.GraphController.GRAPH_TYPE.HOLE_DISTANCE_INVERSE])
            {
                graph = Graphs.GraphController.Graphics[Graphs.GraphController.GRAPH_TYPE.HOLE_DISTANCE];
            }
            else return;
            graph.DrawWithRescale();
        }

        private void BtnSetMaximum_Click(object sender, EventArgs e)
        {
            string s = tboxMaxValueChart.Text;
            s = s.Replace(".", ",");
            Graphs.Graph graph = Graphs.GraphController.GetIsDrawGraph(Graphs.GraphController.GraphWrappers[Graphs.GraphController.CHART_TYPE.HOLE]);
            if (graph == null) return;
            double dValue = 0.0;
            try
            {
                dValue = Convert.ToDouble(s);
            }
            catch (Exception ex)
            {
                return;
            }
            graph.SetMax(graph.AxisX, dValue);
        }

        private void BtnDecMinimal_Click(object sender, EventArgs e)
        {
            Graphs.Graph graph = Graphs.GraphController.GetIsDrawGraph(Graphs.GraphController.GraphWrappers[Graphs.GraphController.CHART_TYPE.HOLE]);
            if (graph == null) return;
            graph.SetMin(graph.AxisX, graph.AxisX.Min - graph.AxisX.Grid);
        }

        private void BtnIncMinimal_Click(object sender, EventArgs e)
        {
            Graphs.Graph graph = Graphs.GraphController.GetIsDrawGraph(Graphs.GraphController.GraphWrappers[Graphs.GraphController.CHART_TYPE.HOLE]);
            if (graph == null) return;
            graph.SetMin(graph.AxisX, graph.AxisX.Min + graph.AxisX.Grid);
        }

        private void BtnDecMaximum_Click(object sender, EventArgs e)
        {
            Graphs.Graph graph = Graphs.GraphController.GetIsDrawGraph(Graphs.GraphController.GraphWrappers[Graphs.GraphController.CHART_TYPE.HOLE]);
            if (graph == null) return;
            graph.SetMax(graph.AxisX, graph.AxisX.Max - graph.AxisX.Grid);
        }

        private void BtnIncMaximum_Click(object sender, EventArgs e)
        {
            Graphs.Graph graph = Graphs.GraphController.GetIsDrawGraph(Graphs.GraphController.GraphWrappers[Graphs.GraphController.CHART_TYPE.HOLE]);
            if (graph == null) return;
            graph.SetMax(graph.AxisX, graph.AxisX.Max + graph.AxisX.Grid);
        }

        private void BtnSetMinRssi_Click(object sender, EventArgs e)
        {
            string s = tboxMinRssiVal.Text;
            s = s.Replace(".", ",");

            Graphs.Graph graph = Graphs.GraphController.GetIsDrawGraph(Graphs.GraphController.GraphWrappers[Graphs.GraphController.CHART_TYPE.RSSI]);
            if (graph == null) return;
            double dValue = 0.0;
            try
            {
                dValue = Convert.ToDouble(s);
            }
            catch (Exception ex)
            {
                return;
            }
            graph.SetMin(graph.AxisY, dValue);
        }

        private void BtnSetMaxRssi_Click(object sender, EventArgs e)
        {
            string s = tboxMaxRssiVal.Text;
            s = s.Replace(".", ",");
            Graphs.Graph graph = Graphs.GraphController.GetIsDrawGraph(Graphs.GraphController.GraphWrappers[Graphs.GraphController.CHART_TYPE.RSSI]);
            if (graph == null) return;
            double dValue = 0.0;
            try
            {
                dValue = Convert.ToDouble(s);
            }
            catch (Exception ex)
            {
                return;
            }
            graph.SetMax(graph.AxisY, dValue);
        }

        private void BtnDecMinRssi_Click(object sender, EventArgs e)
        {
            Graphs.Graph graph = Graphs.GraphController.GetIsDrawGraph(Graphs.GraphController.GraphWrappers[Graphs.GraphController.CHART_TYPE.RSSI]);
            if (graph == null) return;
            graph.SetMin(graph.AxisY, graph.AxisY.Min - graph.AxisY.Grid);
        }

        private void BtnIncMinRssi_Click(object sender, EventArgs e)
        {
            Graphs.Graph graph = Graphs.GraphController.GetIsDrawGraph(Graphs.GraphController.GraphWrappers[Graphs.GraphController.CHART_TYPE.RSSI]);
            if (graph == null) return;
            graph.SetMin(graph.AxisY, graph.AxisY.Min + graph.AxisY.Grid);
        }

        private void BtnDecMaxRssi_Click(object sender, EventArgs e)
        {
            Graphs.Graph graph = Graphs.GraphController.GetIsDrawGraph(Graphs.GraphController.GraphWrappers[Graphs.GraphController.CHART_TYPE.RSSI]);
            if (graph == null) return;
            graph.SetMax(graph.AxisY, graph.AxisY.Max - graph.AxisY.Grid);
        }

        private void BtnIncMaxRssi_Click(object sender, EventArgs e)
        {
            Graphs.Graph graph = Graphs.GraphController.GetIsDrawGraph(Graphs.GraphController.GraphWrappers[Graphs.GraphController.CHART_TYPE.RSSI]);
            if (graph == null) return;
            graph.SetMax(graph.AxisY, graph.AxisY.Max + graph.AxisY.Grid);
        }
    }


}
