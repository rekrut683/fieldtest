﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms.DataVisualization.Charting;

namespace FieldTest
{
    namespace Graphs
    {
        delegate void UpdateChartDoubleValue(Chart _chart, Axis _axis, double value);
        delegate void UpdateChartVoid(Chart chart);
        delegate object UpdateChartSeries(Chart chart, string name);

        delegate void ClearChartSeriesDelegate(Chart chart, Series series);
        delegate void UpdateChartDataDelegate(Chart chart, object[,] data, Series series);
        delegate void AddSeriesChartDelegate(Chart chart, Series series);


        public class SeriesWrapper
        {
            private Series series;
            private GraphWrapper graphWrapper;
            public Series Series
            {
                get { return this.series; }
            }
            public GraphWrapper GetCanvas
            {
                get { return this.graphWrapper; }
            }
            public SeriesWrapper(GraphWrapper graphWrapper, 
                                string name, 
                                SeriesChartType type, 
                                System.Drawing.Color color,
                                int border)
            {
                this.graphWrapper = graphWrapper;
                series = new Series();
                series.BorderWidth = border;
                series.ChartArea = "ChartArea1";
                series.ChartType = type;
                series.Color = color;
                series.Legend = "Legend1";
                series.Name = name;
                series.YValuesPerPoint = 4;
                this.graphWrapper.AddSeries(series);
            }
            public void Clear()
            {
                this.graphWrapper.Clear(this.series);
            }

            public void Update(object[,] data)
            {
                this.graphWrapper.Update(data, this.series);
            }
        }
        public class GraphWrapper
        {
            private Chart _chart;
            private Axis axisY;
            private Axis axisX;
            private Dictionary<string, Series> seriesCollection = new Dictionary<string, Series>();
            private int _currentDrawGraph;

            public GraphWrapper(Chart chart)
            {
                _chart = chart;
                _chart.ChartAreas[0].AxisX.ScaleView.Zoomable = true;
                _chart.ChartAreas[0].AxisY.ScaleView.Zoomable = true;
                _chart.ChartAreas[0].AxisX.Minimum = 0;
                _chart.ChartAreas[0].AxisY.Minimum = 0;
                _chart.ChartAreas[0].AxisX.Maximum = 0.1;
                _chart.ChartAreas[0].AxisY.Maximum = 0.1;
                _chart.Series.Clear();
            }
            public Axis AxisX
            {
                get { return this._chart.ChartAreas[0].AxisX; }
            }
            public Axis AxisY
            {
                get { return this._chart.ChartAreas[0].AxisY; }
            }
            public int CurrentGraph
            {
                get { return this._currentDrawGraph; }
                set { this._currentDrawGraph = value; }
            }

            // clear local series in chart
            public void Clear(Series ser)
            {
                if (_chart == null) return;

                if (_chart.InvokeRequired)
                {
                    _chart.Invoke(new ClearChartSeriesDelegate((c,s) =>
                    {
                        s.Points.Clear();
                    }), new object[] { _chart, ser });

                    return;
                }
                ser.Points.Clear();
                this._currentDrawGraph = 0;
            }

            // fully clear chart
            public void Clear()
            {
                foreach(var ser in _chart.Series)
                {
                    Clear(ser);
                }
            }

            public void AddSeries(Series ser)
            {
                if (_chart == null) return;

                if (_chart.InvokeRequired)
                {
                    _chart.Invoke(new AddSeriesChartDelegate((c, s) =>
                    {
                        c.Series.Add(s);
                    }), new object[] { _chart, ser });
                }
                _chart.Series.Add(ser);
            }

            public void ClearSeries(string name)
            {

            }

            public void SetMax(Axis axis, double value)
            {
                if (_chart == null) return;

                if (_chart.InvokeRequired)
                {
                    _chart.Invoke(new UpdateChartDoubleValue((c, a, d) =>
                    {
                        //c.ChartAreas[0].AxisX.Maximum = d;
                        a.Maximum = d;
                    }), new object[] { _chart, axis, value });

                    return;
                }
                //_chart.ChartAreas[0].AxisX.Maximum = value;
                axis.Maximum = value;
            }

            public void SetMin(Axis axis, double value)
            {
                if (_chart == null) return;
                if (_chart.InvokeRequired)
                {
                    _chart.Invoke(new UpdateChartDoubleValue((c, a, d) =>
                    {
                        a.Minimum = d;
                    }), new object[] { _chart, axis, value });

                    return;
                }
                axis.Minimum = value;
            }

            public void Update(object[,] data, Series series)
            {
                if (_chart == null) return;

                if (_chart.InvokeRequired)
                {
                    _chart.Invoke(new UpdateChartDataDelegate((c, d, s) =>
                    {
                        for (var i = 0; i < d.Length / 2; i++)
                        {
                            s.Points.AddXY(d[i, 0], d[i, 1]);
                        }
                    }), new object[] { _chart, data, series });

                    return;
                }
                for (var i = 0; i < data.Length / 2; i++)
                {
                    series.Points.AddXY(data[i, 0], data[i, 1]);
                }
            }

            void ZoomIn(double value)
            {
                throw new NotImplementedException();
            }

            void ZoomOut(double value)
            {
                throw new NotImplementedException();
            }

            public void SetGridInterval(Axis axis, double value)
            {
                if (_chart == null) return;
                if (_chart.InvokeRequired)
                {
                    _chart.Invoke(new UpdateChartDoubleValue((c, a, d) =>
                    {
                        a.MajorGrid.Interval = d;
                    }), new object[] { _chart, axis, value });

                    return;
                }
                axis.MajorGrid.Interval = value;
            }
        }
    }
}
