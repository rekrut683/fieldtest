﻿namespace FieldTest
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend2 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.cmbSerial = new System.Windows.Forms.ComboBox();
            this.btnRefreshSerial = new System.Windows.Forms.Button();
            this.btnStart = new System.Windows.Forms.Button();
            this.btnStop = new System.Windows.Forms.Button();
            this.lblTitleCntPackets = new System.Windows.Forms.Label();
            this.tboxCntPackets = new System.Windows.Forms.TextBox();
            this.lblTitleChannel = new System.Windows.Forms.Label();
            this.tboxChannel = new System.Windows.Forms.TextBox();
            this.lblTitleInterval = new System.Windows.Forms.Label();
            this.tboxInterval = new System.Windows.Forms.TextBox();
            this.lblSpeed = new System.Windows.Forms.Label();
            this.tboxSpeed = new System.Windows.Forms.TextBox();
            this.lblTitleTimeBegin = new System.Windows.Forms.Label();
            this.lblTitileTimeFinish = new System.Windows.Forms.Label();
            this.lblTimeBegin = new System.Windows.Forms.Label();
            this.lblTimeFinish = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.lblTmeEstimatedSeconds = new System.Windows.Forms.Label();
            this.lblTimeEstimatedMinute = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.lblStatusFinish = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.lblLossCount = new System.Windows.Forms.Label();
            this.lblLossCountTitle = new System.Windows.Forms.Label();
            this.lblFirstId = new System.Windows.Forms.Label();
            this.lblRssi = new System.Windows.Forms.Label();
            this.lblPercent = new System.Windows.Forms.Label();
            this.lblBroken = new System.Windows.Forms.Label();
            this.lblReceived = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.tboxLossIds = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.pBarPackets = new System.Windows.Forms.ProgressBar();
            this.tboxSerial = new System.Windows.Forms.TextBox();
            this.btnDecMinimal = new System.Windows.Forms.Button();
            this.tboxMinValueChart = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.btnIncMinimal = new System.Windows.Forms.Button();
            this.tboxMaxValueChart = new System.Windows.Forms.TextBox();
            this.btnDecMaximum = new System.Windows.Forms.Button();
            this.btnIncMaximum = new System.Windows.Forms.Button();
            this.btnSetMinimal = new System.Windows.Forms.Button();
            this.btnSetMaximum = new System.Windows.Forms.Button();
            this.btnDistance = new System.Windows.Forms.Button();
            this.btnTime = new System.Windows.Forms.Button();
            this.chart1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.btnInverse = new System.Windows.Forms.Button();
            this.gpBoxFuncGraph = new System.Windows.Forms.GroupBox();
            this.btnRestore = new System.Windows.Forms.Button();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.chart2 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.btnIncMinRssi = new System.Windows.Forms.Button();
            this.btnDecMinRssi = new System.Windows.Forms.Button();
            this.tboxMinRssiVal = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.btnSetMinRssi = new System.Windows.Forms.Button();
            this.btnSetMaxRssi = new System.Windows.Forms.Button();
            this.btnIncMaxRssi = new System.Windows.Forms.Button();
            this.label16 = new System.Windows.Forms.Label();
            this.btnDecMaxRssi = new System.Windows.Forms.Button();
            this.tboxMaxRssiVal = new System.Windows.Forms.TextBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
            this.gpBoxFuncGraph.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart2)).BeginInit();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // cmbSerial
            // 
            this.cmbSerial.FormattingEnabled = true;
            this.cmbSerial.Location = new System.Drawing.Point(12, 12);
            this.cmbSerial.Name = "cmbSerial";
            this.cmbSerial.Size = new System.Drawing.Size(121, 21);
            this.cmbSerial.TabIndex = 0;
            // 
            // btnRefreshSerial
            // 
            this.btnRefreshSerial.Location = new System.Drawing.Point(150, 10);
            this.btnRefreshSerial.Name = "btnRefreshSerial";
            this.btnRefreshSerial.Size = new System.Drawing.Size(75, 23);
            this.btnRefreshSerial.TabIndex = 0;
            this.btnRefreshSerial.Text = "Refresh";
            this.btnRefreshSerial.UseVisualStyleBackColor = true;
            this.btnRefreshSerial.Click += new System.EventHandler(this.BtnRefreshSerial_Click);
            // 
            // btnStart
            // 
            this.btnStart.Location = new System.Drawing.Point(236, 54);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(75, 23);
            this.btnStart.TabIndex = 5;
            this.btnStart.Text = "Start";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.BtnStart_Click);
            // 
            // btnStop
            // 
            this.btnStop.Location = new System.Drawing.Point(236, 83);
            this.btnStop.Name = "btnStop";
            this.btnStop.Size = new System.Drawing.Size(75, 23);
            this.btnStop.TabIndex = 6;
            this.btnStop.Text = "Stop";
            this.btnStop.UseVisualStyleBackColor = true;
            this.btnStop.Click += new System.EventHandler(this.BtnStop_Click);
            // 
            // lblTitleCntPackets
            // 
            this.lblTitleCntPackets.AutoSize = true;
            this.lblTitleCntPackets.Location = new System.Drawing.Point(6, 59);
            this.lblTitleCntPackets.Name = "lblTitleCntPackets";
            this.lblTitleCntPackets.Size = new System.Drawing.Size(88, 13);
            this.lblTitleCntPackets.TabIndex = 4;
            this.lblTitleCntPackets.Text = "Кол-во пакетов:";
            // 
            // tboxCntPackets
            // 
            this.tboxCntPackets.Location = new System.Drawing.Point(128, 56);
            this.tboxCntPackets.Name = "tboxCntPackets";
            this.tboxCntPackets.Size = new System.Drawing.Size(40, 20);
            this.tboxCntPackets.TabIndex = 2;
            // 
            // lblTitleChannel
            // 
            this.lblTitleChannel.AutoSize = true;
            this.lblTitleChannel.Location = new System.Drawing.Point(6, 25);
            this.lblTitleChannel.Name = "lblTitleChannel";
            this.lblTitleChannel.Size = new System.Drawing.Size(41, 13);
            this.lblTitleChannel.TabIndex = 6;
            this.lblTitleChannel.Text = "Канал:";
            // 
            // tboxChannel
            // 
            this.tboxChannel.Location = new System.Drawing.Point(128, 22);
            this.tboxChannel.Name = "tboxChannel";
            this.tboxChannel.Size = new System.Drawing.Size(40, 20);
            this.tboxChannel.TabIndex = 1;
            // 
            // lblTitleInterval
            // 
            this.lblTitleInterval.AutoSize = true;
            this.lblTitleInterval.Location = new System.Drawing.Point(6, 92);
            this.lblTitleInterval.Name = "lblTitleInterval";
            this.lblTitleInterval.Size = new System.Drawing.Size(92, 13);
            this.lblTitleInterval.TabIndex = 8;
            this.lblTitleInterval.Text = "Скважность, мс:";
            // 
            // tboxInterval
            // 
            this.tboxInterval.Location = new System.Drawing.Point(128, 89);
            this.tboxInterval.Name = "tboxInterval";
            this.tboxInterval.Size = new System.Drawing.Size(40, 20);
            this.tboxInterval.TabIndex = 3;
            // 
            // lblSpeed
            // 
            this.lblSpeed.AutoSize = true;
            this.lblSpeed.Location = new System.Drawing.Point(6, 127);
            this.lblSpeed.Name = "lblSpeed";
            this.lblSpeed.Size = new System.Drawing.Size(88, 13);
            this.lblSpeed.TabIndex = 10;
            this.lblSpeed.Text = "Скорость, км/ч:";
            // 
            // tboxSpeed
            // 
            this.tboxSpeed.Location = new System.Drawing.Point(128, 124);
            this.tboxSpeed.Name = "tboxSpeed";
            this.tboxSpeed.Size = new System.Drawing.Size(40, 20);
            this.tboxSpeed.TabIndex = 4;
            // 
            // lblTitleTimeBegin
            // 
            this.lblTitleTimeBegin.AutoSize = true;
            this.lblTitleTimeBegin.Location = new System.Drawing.Point(6, 26);
            this.lblTitleTimeBegin.Name = "lblTitleTimeBegin";
            this.lblTitleTimeBegin.Size = new System.Drawing.Size(81, 13);
            this.lblTitleTimeBegin.TabIndex = 12;
            this.lblTitleTimeBegin.Text = "Время начала:";
            // 
            // lblTitileTimeFinish
            // 
            this.lblTitileTimeFinish.AutoSize = true;
            this.lblTitileTimeFinish.Location = new System.Drawing.Point(6, 49);
            this.lblTitileTimeFinish.Name = "lblTitileTimeFinish";
            this.lblTitileTimeFinish.Size = new System.Drawing.Size(99, 13);
            this.lblTitileTimeFinish.TabIndex = 13;
            this.lblTitileTimeFinish.Text = "Время окончания:";
            // 
            // lblTimeBegin
            // 
            this.lblTimeBegin.AutoSize = true;
            this.lblTimeBegin.Location = new System.Drawing.Point(128, 25);
            this.lblTimeBegin.Name = "lblTimeBegin";
            this.lblTimeBegin.Size = new System.Drawing.Size(35, 13);
            this.lblTimeBegin.TabIndex = 14;
            this.lblTimeBegin.Text = "label1";
            // 
            // lblTimeFinish
            // 
            this.lblTimeFinish.AutoSize = true;
            this.lblTimeFinish.Location = new System.Drawing.Point(128, 49);
            this.lblTimeFinish.Name = "lblTimeFinish";
            this.lblTimeFinish.Size = new System.Drawing.Size(35, 13);
            this.lblTimeFinish.TabIndex = 15;
            this.lblTimeFinish.Text = "label2";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lblTitleChannel);
            this.groupBox1.Controls.Add(this.lblTitleCntPackets);
            this.groupBox1.Controls.Add(this.tboxCntPackets);
            this.groupBox1.Controls.Add(this.tboxChannel);
            this.groupBox1.Controls.Add(this.lblTitleInterval);
            this.groupBox1.Controls.Add(this.tboxSpeed);
            this.groupBox1.Controls.Add(this.tboxInterval);
            this.groupBox1.Controls.Add(this.lblSpeed);
            this.groupBox1.Location = new System.Drawing.Point(12, 39);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(200, 155);
            this.groupBox1.TabIndex = 16;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Настройки";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.lblTmeEstimatedSeconds);
            this.groupBox2.Controls.Add(this.lblTimeEstimatedMinute);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.lblTitileTimeFinish);
            this.groupBox2.Controls.Add(this.lblTitleTimeBegin);
            this.groupBox2.Controls.Add(this.lblTimeFinish);
            this.groupBox2.Controls.Add(this.lblTimeBegin);
            this.groupBox2.Location = new System.Drawing.Point(12, 200);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(317, 76);
            this.groupBox2.TabIndex = 17;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Время";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(271, 36);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(25, 13);
            this.label10.TabIndex = 20;
            this.label10.Text = "сек";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(211, 36);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(27, 13);
            this.label9.TabIndex = 19;
            this.label9.Text = "мин";
            // 
            // lblTmeEstimatedSeconds
            // 
            this.lblTmeEstimatedSeconds.AutoSize = true;
            this.lblTmeEstimatedSeconds.Location = new System.Drawing.Point(254, 36);
            this.lblTmeEstimatedSeconds.Name = "lblTmeEstimatedSeconds";
            this.lblTmeEstimatedSeconds.Size = new System.Drawing.Size(19, 13);
            this.lblTmeEstimatedSeconds.TabIndex = 18;
            this.lblTmeEstimatedSeconds.Text = "60";
            // 
            // lblTimeEstimatedMinute
            // 
            this.lblTimeEstimatedMinute.AutoSize = true;
            this.lblTimeEstimatedMinute.Location = new System.Drawing.Point(194, 36);
            this.lblTimeEstimatedMinute.Name = "lblTimeEstimatedMinute";
            this.lblTimeEstimatedMinute.Size = new System.Drawing.Size(19, 13);
            this.lblTimeEstimatedMinute.TabIndex = 17;
            this.lblTimeEstimatedMinute.Text = "60";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(186, 13);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(97, 13);
            this.label8.TabIndex = 16;
            this.label8.Text = "Прошло времени:";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.lblStatusFinish);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this.lblLossCount);
            this.groupBox3.Controls.Add(this.lblLossCountTitle);
            this.groupBox3.Controls.Add(this.lblFirstId);
            this.groupBox3.Controls.Add(this.lblRssi);
            this.groupBox3.Controls.Add(this.lblPercent);
            this.groupBox3.Controls.Add(this.lblBroken);
            this.groupBox3.Controls.Add(this.lblReceived);
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Controls.Add(this.tboxLossIds);
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.Controls.Add(this.label2);
            this.groupBox3.Controls.Add(this.label1);
            this.groupBox3.Location = new System.Drawing.Point(12, 310);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(317, 413);
            this.groupBox3.TabIndex = 18;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Статистика";
            // 
            // lblStatusFinish
            // 
            this.lblStatusFinish.AutoSize = true;
            this.lblStatusFinish.Location = new System.Drawing.Point(164, 190);
            this.lblStatusFinish.Name = "lblStatusFinish";
            this.lblStatusFinish.Size = new System.Drawing.Size(41, 13);
            this.lblStatusFinish.TabIndex = 26;
            this.lblStatusFinish.Text = "label11";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(7, 190);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(100, 13);
            this.label7.TabIndex = 25;
            this.label7.Text = "Статус окончания:";
            // 
            // lblLossCount
            // 
            this.lblLossCount.AutoSize = true;
            this.lblLossCount.Location = new System.Drawing.Point(164, 167);
            this.lblLossCount.Name = "lblLossCount";
            this.lblLossCount.Size = new System.Drawing.Size(41, 13);
            this.lblLossCount.TabIndex = 24;
            this.lblLossCount.Text = "label11";
            // 
            // lblLossCountTitle
            // 
            this.lblLossCountTitle.AutoSize = true;
            this.lblLossCountTitle.Location = new System.Drawing.Point(6, 167);
            this.lblLossCountTitle.Name = "lblLossCountTitle";
            this.lblLossCountTitle.Size = new System.Drawing.Size(151, 13);
            this.lblLossCountTitle.TabIndex = 23;
            this.lblLossCountTitle.Text = "Кол-во потерянных пакетов:";
            // 
            // lblFirstId
            // 
            this.lblFirstId.AutoSize = true;
            this.lblFirstId.Location = new System.Drawing.Point(164, 145);
            this.lblFirstId.Name = "lblFirstId";
            this.lblFirstId.Size = new System.Drawing.Size(41, 13);
            this.lblFirstId.TabIndex = 22;
            this.lblFirstId.Text = "label11";
            // 
            // lblRssi
            // 
            this.lblRssi.AutoSize = true;
            this.lblRssi.Location = new System.Drawing.Point(163, 123);
            this.lblRssi.Name = "lblRssi";
            this.lblRssi.Size = new System.Drawing.Size(41, 13);
            this.lblRssi.TabIndex = 21;
            this.lblRssi.Text = "label10";
            // 
            // lblPercent
            // 
            this.lblPercent.AutoSize = true;
            this.lblPercent.Location = new System.Drawing.Point(163, 98);
            this.lblPercent.Name = "lblPercent";
            this.lblPercent.Size = new System.Drawing.Size(35, 13);
            this.lblPercent.TabIndex = 20;
            this.lblPercent.Text = "label9";
            // 
            // lblBroken
            // 
            this.lblBroken.AutoSize = true;
            this.lblBroken.Location = new System.Drawing.Point(163, 74);
            this.lblBroken.Name = "lblBroken";
            this.lblBroken.Size = new System.Drawing.Size(35, 13);
            this.lblBroken.TabIndex = 19;
            this.lblBroken.Text = "label8";
            // 
            // lblReceived
            // 
            this.lblReceived.AutoSize = true;
            this.lblReceived.Location = new System.Drawing.Point(163, 51);
            this.lblReceived.Name = "lblReceived";
            this.lblReceived.Size = new System.Drawing.Size(35, 13);
            this.lblReceived.TabIndex = 18;
            this.lblReceived.Text = "label7";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 145);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(62, 13);
            this.label6.TabIndex = 17;
            this.label6.Text = "Первый Id:";
            // 
            // tboxLossIds
            // 
            this.tboxLossIds.ForeColor = System.Drawing.SystemColors.WindowText;
            this.tboxLossIds.Location = new System.Drawing.Point(9, 310);
            this.tboxLossIds.Multiline = true;
            this.tboxLossIds.Name = "tboxLossIds";
            this.tboxLossIds.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tboxLossIds.Size = new System.Drawing.Size(302, 97);
            this.tboxLossIds.TabIndex = 16;
            this.tboxLossIds.TextChanged += new System.EventHandler(this.TboxLossIds_TextChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 294);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(126, 13);
            this.label5.TabIndex = 15;
            this.label5.Text = "Id потерянных пакетов:";
            this.label5.Click += new System.EventHandler(this.Label5_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 123);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(86, 13);
            this.label4.TabIndex = 14;
            this.label4.Text = "RSSI принятых:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 98);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(94, 13);
            this.label3.TabIndex = 13;
            this.label3.Text = "Процент приема:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 74);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(82, 13);
            this.label2.TabIndex = 12;
            this.label2.Text = "Битые пакеты:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 51);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(101, 13);
            this.label1.TabIndex = 11;
            this.label1.Text = "Принятые пакеты:";
            // 
            // pBarPackets
            // 
            this.pBarPackets.Location = new System.Drawing.Point(13, 729);
            this.pBarPackets.Name = "pBarPackets";
            this.pBarPackets.Size = new System.Drawing.Size(298, 23);
            this.pBarPackets.Step = 1;
            this.pBarPackets.TabIndex = 23;
            // 
            // tboxSerial
            // 
            this.tboxSerial.Location = new System.Drawing.Point(13, 758);
            this.tboxSerial.Multiline = true;
            this.tboxSerial.Name = "tboxSerial";
            this.tboxSerial.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tboxSerial.Size = new System.Drawing.Size(310, 153);
            this.tboxSerial.TabIndex = 24;
            this.tboxSerial.TextChanged += new System.EventHandler(this.TboxSerial_TextChanged);
            // 
            // btnDecMinimal
            // 
            this.btnDecMinimal.Location = new System.Drawing.Point(39, 45);
            this.btnDecMinimal.Name = "btnDecMinimal";
            this.btnDecMinimal.Size = new System.Drawing.Size(24, 23);
            this.btnDecMinimal.TabIndex = 26;
            this.btnDecMinimal.Text = "-";
            this.btnDecMinimal.UseVisualStyleBackColor = true;
            this.btnDecMinimal.Click += new System.EventHandler(this.BtnDecMinimal_Click);
            // 
            // tboxMinValueChart
            // 
            this.tboxMinValueChart.Location = new System.Drawing.Point(39, 19);
            this.tboxMinValueChart.Name = "tboxMinValueChart";
            this.tboxMinValueChart.Size = new System.Drawing.Size(54, 20);
            this.tboxMinValueChart.TabIndex = 28;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(11, 22);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(26, 13);
            this.label11.TabIndex = 27;
            this.label11.Text = "min:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(103, 22);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(29, 13);
            this.label12.TabIndex = 29;
            this.label12.Text = "max:";
            // 
            // btnIncMinimal
            // 
            this.btnIncMinimal.Location = new System.Drawing.Point(69, 45);
            this.btnIncMinimal.Name = "btnIncMinimal";
            this.btnIncMinimal.Size = new System.Drawing.Size(24, 23);
            this.btnIncMinimal.TabIndex = 31;
            this.btnIncMinimal.Text = "+";
            this.btnIncMinimal.UseVisualStyleBackColor = true;
            this.btnIncMinimal.Click += new System.EventHandler(this.BtnIncMinimal_Click);
            // 
            // tboxMaxValueChart
            // 
            this.tboxMaxValueChart.Location = new System.Drawing.Point(134, 19);
            this.tboxMaxValueChart.Name = "tboxMaxValueChart";
            this.tboxMaxValueChart.Size = new System.Drawing.Size(54, 20);
            this.tboxMaxValueChart.TabIndex = 33;
            // 
            // btnDecMaximum
            // 
            this.btnDecMaximum.Location = new System.Drawing.Point(134, 45);
            this.btnDecMaximum.Name = "btnDecMaximum";
            this.btnDecMaximum.Size = new System.Drawing.Size(24, 23);
            this.btnDecMaximum.TabIndex = 34;
            this.btnDecMaximum.Text = "-";
            this.btnDecMaximum.UseVisualStyleBackColor = true;
            this.btnDecMaximum.Click += new System.EventHandler(this.BtnDecMaximum_Click);
            // 
            // btnIncMaximum
            // 
            this.btnIncMaximum.Location = new System.Drawing.Point(164, 45);
            this.btnIncMaximum.Name = "btnIncMaximum";
            this.btnIncMaximum.Size = new System.Drawing.Size(24, 23);
            this.btnIncMaximum.TabIndex = 35;
            this.btnIncMaximum.Text = "+";
            this.btnIncMaximum.UseVisualStyleBackColor = true;
            this.btnIncMaximum.Click += new System.EventHandler(this.BtnIncMaximum_Click);
            // 
            // btnSetMinimal
            // 
            this.btnSetMinimal.Location = new System.Drawing.Point(39, 74);
            this.btnSetMinimal.Name = "btnSetMinimal";
            this.btnSetMinimal.Size = new System.Drawing.Size(54, 23);
            this.btnSetMinimal.TabIndex = 36;
            this.btnSetMinimal.Text = "Set";
            this.btnSetMinimal.UseVisualStyleBackColor = true;
            this.btnSetMinimal.Click += new System.EventHandler(this.BtnSetMinimal_Click);
            // 
            // btnSetMaximum
            // 
            this.btnSetMaximum.Location = new System.Drawing.Point(134, 74);
            this.btnSetMaximum.Name = "btnSetMaximum";
            this.btnSetMaximum.Size = new System.Drawing.Size(54, 23);
            this.btnSetMaximum.TabIndex = 37;
            this.btnSetMaximum.Text = "Set";
            this.btnSetMaximum.UseVisualStyleBackColor = true;
            this.btnSetMaximum.Click += new System.EventHandler(this.BtnSetMaximum_Click);
            // 
            // btnDistance
            // 
            this.btnDistance.Location = new System.Drawing.Point(1209, 342);
            this.btnDistance.Name = "btnDistance";
            this.btnDistance.Size = new System.Drawing.Size(75, 23);
            this.btnDistance.TabIndex = 38;
            this.btnDistance.Text = "Расстояние";
            this.btnDistance.UseVisualStyleBackColor = true;
            this.btnDistance.Click += new System.EventHandler(this.BtnDistance_Click);
            // 
            // btnTime
            // 
            this.btnTime.Location = new System.Drawing.Point(1209, 313);
            this.btnTime.Name = "btnTime";
            this.btnTime.Size = new System.Drawing.Size(75, 23);
            this.btnTime.TabIndex = 39;
            this.btnTime.Text = "Время";
            this.btnTime.UseVisualStyleBackColor = true;
            this.btnTime.Click += new System.EventHandler(this.BtnTime_Click);
            // 
            // chart1
            // 
            chartArea1.AxisX.LabelStyle.Format = "F2";
            chartArea1.AxisX.MajorGrid.Interval = 0D;
            chartArea1.Name = "ChartArea1";
            this.chart1.ChartAreas.Add(chartArea1);
            legend1.Name = "Legend1";
            this.chart1.Legends.Add(legend1);
            this.chart1.Location = new System.Drawing.Point(399, 39);
            this.chart1.Name = "chart1";
            series1.BorderWidth = 5;
            series1.ChartArea = "ChartArea1";
            series1.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.StepLine;
            series1.Legend = "Legend1";
            series1.Name = "Series1";
            series1.YValuesPerPoint = 4;
            this.chart1.Series.Add(series1);
            this.chart1.Size = new System.Drawing.Size(885, 262);
            this.chart1.TabIndex = 25;
            this.chart1.Text = "chart1";
            // 
            // btnInverse
            // 
            this.btnInverse.Location = new System.Drawing.Point(214, 16);
            this.btnInverse.Name = "btnInverse";
            this.btnInverse.Size = new System.Drawing.Size(94, 23);
            this.btnInverse.TabIndex = 40;
            this.btnInverse.Text = "Инвертировать";
            this.btnInverse.UseVisualStyleBackColor = true;
            this.btnInverse.Click += new System.EventHandler(this.BtnInverse_Click);
            // 
            // gpBoxFuncGraph
            // 
            this.gpBoxFuncGraph.Controls.Add(this.btnRestore);
            this.gpBoxFuncGraph.Controls.Add(this.btnIncMinimal);
            this.gpBoxFuncGraph.Controls.Add(this.btnInverse);
            this.gpBoxFuncGraph.Controls.Add(this.btnDecMinimal);
            this.gpBoxFuncGraph.Controls.Add(this.tboxMinValueChart);
            this.gpBoxFuncGraph.Controls.Add(this.label11);
            this.gpBoxFuncGraph.Controls.Add(this.btnSetMinimal);
            this.gpBoxFuncGraph.Controls.Add(this.btnSetMaximum);
            this.gpBoxFuncGraph.Controls.Add(this.btnIncMaximum);
            this.gpBoxFuncGraph.Controls.Add(this.label12);
            this.gpBoxFuncGraph.Controls.Add(this.btnDecMaximum);
            this.gpBoxFuncGraph.Controls.Add(this.tboxMaxValueChart);
            this.gpBoxFuncGraph.Location = new System.Drawing.Point(865, 313);
            this.gpBoxFuncGraph.Name = "gpBoxFuncGraph";
            this.gpBoxFuncGraph.Size = new System.Drawing.Size(322, 137);
            this.gpBoxFuncGraph.TabIndex = 41;
            this.gpBoxFuncGraph.TabStop = false;
            this.gpBoxFuncGraph.Text = "Действия";
            // 
            // btnRestore
            // 
            this.btnRestore.Location = new System.Drawing.Point(214, 45);
            this.btnRestore.Name = "btnRestore";
            this.btnRestore.Size = new System.Drawing.Size(94, 23);
            this.btnRestore.TabIndex = 41;
            this.btnRestore.Text = "Восстановить";
            this.btnRestore.UseVisualStyleBackColor = true;
            this.btnRestore.Click += new System.EventHandler(this.BtnRestore_Click);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label13.Location = new System.Drawing.Point(395, 7);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(94, 24);
            this.label13.TabIndex = 21;
            this.label13.Text = "ПАКЕТЫ";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label14.Location = new System.Drawing.Point(395, 466);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(55, 24);
            this.label14.TabIndex = 42;
            this.label14.Text = "RSSI";
            // 
            // chart2
            // 
            chartArea2.AxisX.LabelStyle.Format = "F2";
            chartArea2.AxisX.MajorGrid.Interval = 0D;
            chartArea2.AxisX.MajorGrid.LineColor = System.Drawing.Color.Green;
            chartArea2.AxisX.MajorTickMark.LineColor = System.Drawing.Color.Green;
            chartArea2.AxisY.MajorGrid.LineColor = System.Drawing.Color.Green;
            chartArea2.Name = "ChartArea1";
            this.chart2.ChartAreas.Add(chartArea2);
            legend2.Name = "Legend1";
            this.chart2.Legends.Add(legend2);
            this.chart2.Location = new System.Drawing.Point(399, 500);
            this.chart2.Name = "chart2";
            series2.BorderWidth = 5;
            series2.ChartArea = "ChartArea1";
            series2.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.StepLine;
            series2.Legend = "Legend1";
            series2.Name = "Series1";
            series2.YValuesPerPoint = 4;
            this.chart2.Series.Add(series2);
            this.chart2.Size = new System.Drawing.Size(885, 262);
            this.chart2.TabIndex = 43;
            this.chart2.Text = "chart2";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.btnIncMinRssi);
            this.groupBox4.Controls.Add(this.btnDecMinRssi);
            this.groupBox4.Controls.Add(this.tboxMinRssiVal);
            this.groupBox4.Controls.Add(this.label15);
            this.groupBox4.Controls.Add(this.btnSetMinRssi);
            this.groupBox4.Controls.Add(this.btnSetMaxRssi);
            this.groupBox4.Controls.Add(this.btnIncMaxRssi);
            this.groupBox4.Controls.Add(this.label16);
            this.groupBox4.Controls.Add(this.btnDecMaxRssi);
            this.groupBox4.Controls.Add(this.tboxMaxRssiVal);
            this.groupBox4.Location = new System.Drawing.Point(865, 774);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(211, 137);
            this.groupBox4.TabIndex = 44;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Действия";
            // 
            // btnIncMinRssi
            // 
            this.btnIncMinRssi.Location = new System.Drawing.Point(69, 45);
            this.btnIncMinRssi.Name = "btnIncMinRssi";
            this.btnIncMinRssi.Size = new System.Drawing.Size(24, 23);
            this.btnIncMinRssi.TabIndex = 31;
            this.btnIncMinRssi.Text = "+";
            this.btnIncMinRssi.UseVisualStyleBackColor = true;
            this.btnIncMinRssi.Click += new System.EventHandler(this.BtnIncMinRssi_Click);
            // 
            // btnDecMinRssi
            // 
            this.btnDecMinRssi.Location = new System.Drawing.Point(39, 45);
            this.btnDecMinRssi.Name = "btnDecMinRssi";
            this.btnDecMinRssi.Size = new System.Drawing.Size(24, 23);
            this.btnDecMinRssi.TabIndex = 26;
            this.btnDecMinRssi.Text = "-";
            this.btnDecMinRssi.UseVisualStyleBackColor = true;
            this.btnDecMinRssi.Click += new System.EventHandler(this.BtnDecMinRssi_Click);
            // 
            // tboxMinRssiVal
            // 
            this.tboxMinRssiVal.Location = new System.Drawing.Point(39, 19);
            this.tboxMinRssiVal.Name = "tboxMinRssiVal";
            this.tboxMinRssiVal.Size = new System.Drawing.Size(54, 20);
            this.tboxMinRssiVal.TabIndex = 28;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(11, 22);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(26, 13);
            this.label15.TabIndex = 27;
            this.label15.Text = "min:";
            // 
            // btnSetMinRssi
            // 
            this.btnSetMinRssi.Location = new System.Drawing.Point(39, 74);
            this.btnSetMinRssi.Name = "btnSetMinRssi";
            this.btnSetMinRssi.Size = new System.Drawing.Size(54, 23);
            this.btnSetMinRssi.TabIndex = 36;
            this.btnSetMinRssi.Text = "Set";
            this.btnSetMinRssi.UseVisualStyleBackColor = true;
            this.btnSetMinRssi.Click += new System.EventHandler(this.BtnSetMinRssi_Click);
            // 
            // btnSetMaxRssi
            // 
            this.btnSetMaxRssi.Location = new System.Drawing.Point(134, 74);
            this.btnSetMaxRssi.Name = "btnSetMaxRssi";
            this.btnSetMaxRssi.Size = new System.Drawing.Size(54, 23);
            this.btnSetMaxRssi.TabIndex = 37;
            this.btnSetMaxRssi.Text = "Set";
            this.btnSetMaxRssi.UseVisualStyleBackColor = true;
            this.btnSetMaxRssi.Click += new System.EventHandler(this.BtnSetMaxRssi_Click);
            // 
            // btnIncMaxRssi
            // 
            this.btnIncMaxRssi.Location = new System.Drawing.Point(164, 45);
            this.btnIncMaxRssi.Name = "btnIncMaxRssi";
            this.btnIncMaxRssi.Size = new System.Drawing.Size(24, 23);
            this.btnIncMaxRssi.TabIndex = 35;
            this.btnIncMaxRssi.Text = "+";
            this.btnIncMaxRssi.UseVisualStyleBackColor = true;
            this.btnIncMaxRssi.Click += new System.EventHandler(this.BtnIncMaxRssi_Click);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(103, 22);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(29, 13);
            this.label16.TabIndex = 29;
            this.label16.Text = "max:";
            // 
            // btnDecMaxRssi
            // 
            this.btnDecMaxRssi.Location = new System.Drawing.Point(134, 45);
            this.btnDecMaxRssi.Name = "btnDecMaxRssi";
            this.btnDecMaxRssi.Size = new System.Drawing.Size(24, 23);
            this.btnDecMaxRssi.TabIndex = 34;
            this.btnDecMaxRssi.Text = "-";
            this.btnDecMaxRssi.UseVisualStyleBackColor = true;
            this.btnDecMaxRssi.Click += new System.EventHandler(this.BtnDecMaxRssi_Click);
            // 
            // tboxMaxRssiVal
            // 
            this.tboxMaxRssiVal.Location = new System.Drawing.Point(134, 19);
            this.tboxMaxRssiVal.Name = "tboxMaxRssiVal";
            this.tboxMaxRssiVal.Size = new System.Drawing.Size(54, 20);
            this.tboxMaxRssiVal.TabIndex = 33;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(1321, 1081);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.chart2);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.gpBoxFuncGraph);
            this.Controls.Add(this.chart1);
            this.Controls.Add(this.btnTime);
            this.Controls.Add(this.btnDistance);
            this.Controls.Add(this.tboxSerial);
            this.Controls.Add(this.pBarPackets);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btnStop);
            this.Controls.Add(this.btnStart);
            this.Controls.Add(this.btnRefreshSerial);
            this.Controls.Add(this.cmbSerial);
            this.KeyPreview = true;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form1_FormClosed);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Form1_KeyDown);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Form1_KeyPress);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
            this.gpBoxFuncGraph.ResumeLayout(false);
            this.gpBoxFuncGraph.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart2)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cmbSerial;
        private System.Windows.Forms.Button btnRefreshSerial;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.Button btnStop;
        private System.Windows.Forms.Label lblTitleCntPackets;
        private System.Windows.Forms.TextBox tboxCntPackets;
        private System.Windows.Forms.Label lblTitleChannel;
        private System.Windows.Forms.TextBox tboxChannel;
        private System.Windows.Forms.Label lblTitleInterval;
        private System.Windows.Forms.TextBox tboxInterval;
        private System.Windows.Forms.Label lblSpeed;
        private System.Windows.Forms.TextBox tboxSpeed;
        private System.Windows.Forms.Label lblTitleTimeBegin;
        private System.Windows.Forms.Label lblTitileTimeFinish;
        public System.Windows.Forms.Label lblTimeBegin;
        public System.Windows.Forms.Label lblTimeFinish;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label6;
        public System.Windows.Forms.TextBox tboxLossIds;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.Label lblFirstId;
        public System.Windows.Forms.Label lblRssi;
        public System.Windows.Forms.Label lblPercent;
        public System.Windows.Forms.Label lblBroken;
        public System.Windows.Forms.Label lblReceived;
        public System.Windows.Forms.ProgressBar pBarPackets;
        public System.Windows.Forms.Label lblLossCountTitle;
        public System.Windows.Forms.Label lblLossCount;
        public System.Windows.Forms.Label label7;
        public System.Windows.Forms.Label lblStatusFinish;
        private System.Windows.Forms.Label label8;
        public System.Windows.Forms.Label lblTimeEstimatedMinute;
        public System.Windows.Forms.Label lblTmeEstimatedSeconds;
        public System.Windows.Forms.Label label10;
        public System.Windows.Forms.Label label9;
        public System.Windows.Forms.TextBox tboxSerial;
        private System.Windows.Forms.Button btnDecMinimal;
        private System.Windows.Forms.TextBox tboxMinValueChart;
        public System.Windows.Forms.Label label11;
        public System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button btnIncMinimal;
        private System.Windows.Forms.TextBox tboxMaxValueChart;
        private System.Windows.Forms.Button btnDecMaximum;
        private System.Windows.Forms.Button btnIncMaximum;
        private System.Windows.Forms.Button btnSetMinimal;
        private System.Windows.Forms.Button btnSetMaximum;
        private System.Windows.Forms.Button btnDistance;
        private System.Windows.Forms.Button btnTime;
        public System.Windows.Forms.DataVisualization.Charting.Chart chart1;
        private System.Windows.Forms.Button btnInverse;
        private System.Windows.Forms.GroupBox gpBoxFuncGraph;
        private System.Windows.Forms.Button btnRestore;
        public System.Windows.Forms.Label label13;
        public System.Windows.Forms.Label label14;
        public System.Windows.Forms.DataVisualization.Charting.Chart chart2;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Button btnIncMinRssi;
        private System.Windows.Forms.Button btnDecMinRssi;
        private System.Windows.Forms.TextBox tboxMinRssiVal;
        public System.Windows.Forms.Label label15;
        private System.Windows.Forms.Button btnSetMinRssi;
        private System.Windows.Forms.Button btnSetMaxRssi;
        private System.Windows.Forms.Button btnIncMaxRssi;
        public System.Windows.Forms.Label label16;
        private System.Windows.Forms.Button btnDecMaxRssi;
        private System.Windows.Forms.TextBox tboxMaxRssiVal;
    }
}

