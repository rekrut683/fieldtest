﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms.DataVisualization.Charting;

namespace FieldTest
{

    public class GraphController
    {
    }

}
        /*
        public enum TYPE_GRAPH
        {
            NONE,
            TIME,
            DISTANCE,
            RSSI
        }

        public enum AXIS_TYPE
        {
            X,
            Y
        }

        //public static GraphController PacketsGraph = new GraphController(TYPE_GRAPH.TIME, TYPE_GRAPH.DISTANCE);
        public static GraphController RssiGraph = new GraphController(TYPE_GRAPH.RSSI);

        //private static GraphController _controller;
        private IGraph viewer;
        Dictionary<TYPE_GRAPH, GraphAbs> listGraphs;

        
        private TYPE_GRAPH _activeGraph = TYPE_GRAPH.NONE;
        
        public void SetGraphViewer(IGraph graph)
        {
            viewer = graph;
        }

        public GraphController(params TYPE_GRAPH[] args)
        {
            listGraphs = new Dictionary<TYPE_GRAPH, GraphAbs>();
            foreach(var p in args)
            {
                listGraphs.Add((TYPE_GRAPH)p, new GraphAbs());
            }
        }

        public GraphAbs GetGraph(TYPE_GRAPH type)
        {
            return listGraphs[type];
        }

        public GraphAbs GetActiveGraph()
        {
            if(_activeGraph != TYPE_GRAPH.NONE)
            {
                return listGraphs[_activeGraph];
            }
            return null;
        }

        public void Draw(GraphAbs graph)
        {
            viewer.Clear();
            viewer.SetMinX(graph.Min);
            viewer.SetMaxX(graph.Max);
            viewer.SetGridInterval(graph.Grid);
            viewer.Update(graph.ToArray());

            foreach(var g in listGraphs)
            {
                if (g.Value == graph) _activeGraph = g.Key;
            }
        }

        public void TurnGraph(GraphAbs graph)
        {
            graph.Inverse();
            Draw(graph);

        }

        public void Inverse()
        {
            GraphAbs gr = GetActiveGraph();
            if (gr == null) return;

            if(gr.IsOriginal)
            {
                TurnGraph(gr);
            }
            
        }

        public void Restore()
        {
            GraphAbs gr = GetActiveGraph();
            if (gr == null) return;

            if (!gr.IsOriginal)
            {
                TurnGraph(gr);
            }
        }

        public void SetMinX(string value)
        {
            double dbValue = 0.0;
            string sVal = value.Replace(".", ",");
            try
            {
                dbValue = Convert.ToDouble(value);
                dbValue = Math.Round(dbValue, 2);
            }
            catch(Exception ex)
            {
                return;
            }
            if (dbValue < 0) dbValue = 0;
            GraphAbs graph = GetActiveGraph();

            if (dbValue >= graph.CurMaximum) dbValue -= graph.Grid;
            if (dbValue <= graph.Min) dbValue = graph.Min;

            graph.CurMinumum = dbValue;
            viewer.SetMinX(dbValue);
        }

        public void SetMaxX(string value)
        {
            double dbvalue = 0.0;
            string sVal = value.Replace(".", ",");
            try
            {
                dbvalue = Convert.ToDouble(sVal);
                dbvalue = Math.Round(dbvalue, 2);
            }
            catch (Exception ex)
            {
                return;
            }
            GraphAbs graph = GetActiveGraph();
            if (dbvalue <= graph.CurMinumum) dbvalue = graph.CurMinumum + graph.Grid;
            if (dbvalue >= graph.Max) dbvalue = graph.Max;
            graph.CurMaximum = dbvalue;
            viewer.SetMaxX(dbvalue);
        }

        public void SetMinY(string value)
        {
            double dbValue = 0.0;
            string sVal = value.Replace(".", ",");
            try
            {
                dbValue = Convert.ToDouble(sVal);
                dbValue = Math.Round(dbValue, 2);
            }
            catch (Exception ex)
            {
                return;
            }
            // if (dbValue < 0) dbValue = 0;
            GraphAbs graph = GetActiveGraph();

            if (dbValue >= graph.YCurMaximum) dbValue -= graph.YGrid;
            if (dbValue <= graph.YMin) dbValue = graph.YMin;

            graph.YCurMinimum = dbValue;
            viewer.SetMinY(dbValue);
        }

        public void SetMaxY(string value)
        {
            double dbvalue = 0.0;
            string sVal = value.Replace(".", ",");
            try
            {
                dbvalue = Convert.ToDouble(sVal);
                dbvalue = Math.Round(dbvalue, 2);
            }
            catch (Exception ex)
            {
                return;
            }
            GraphAbs graph = GetActiveGraph();
            if (Math.Abs(dbvalue) <= graph.YCurMinimum) dbvalue = graph.YCurMinimum + graph.YGrid;
            if (dbvalue >= graph.YMax) dbvalue = graph.YMax;
            graph.YCurMaximum = dbvalue;
            viewer.SetMaxY(dbvalue);
        }

        public void ClearView()
        {
            viewer.Clear();
        }

        public void OnlineGraph(double x)
        {
            GraphAbs graph = GraphController.RssiGraph.GetActiveGraph();
            graph.AddOnlinePoint(x);
            GraphController.RssiGraph.Draw(graph);
        }

        public void ZoomInX(GraphAbs graph)
        {
            if (graph == null) return;
            double sMin = graph.CurMinumum;
            double sMax = graph.CurMaximum;

            SetMinX((sMin + graph.Grid).ToString());
            SetMaxX((sMax - graph.Grid).ToString());

        }

        public void ZoomOutX(GraphAbs graph)
        {
            if (graph == null) return;
            double sMin = graph.CurMinumum;
            double sMax = graph.CurMaximum;

            SetMinX((sMin - graph.Grid).ToString());
            SetMaxX((sMax + graph.Grid).ToString());
        }
        */
//    }

//}
