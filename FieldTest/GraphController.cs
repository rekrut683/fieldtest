﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FieldTest
{
    namespace Graphs
    {
        public class GraphController
        {
            public enum GRAPH_TYPE
            {
                NONE,
                HOLE_TIME,
                HOLE_DISTANCE,
                HOLE_TIME_INVERSE,
                HOLE_DISTANCE_INVERSE,
                RSSI_GOOD,
                RSSI_BAD
            }

            public enum CHART_TYPE
            {
                NONE,
                HOLE,
                RSSI
            }

            public static Dictionary<GRAPH_TYPE, Graphs.Graph> Graphics = new Dictionary<GRAPH_TYPE, Graph>();
            public static Dictionary<CHART_TYPE, Graphs.GraphWrapper> GraphWrappers = new Dictionary<CHART_TYPE, GraphWrapper>();

            public static Graphs.Graph GetIsDrawGraph(GraphWrapper wrapper)
            {
                foreach(var kp in Graphics)
                {
                    if(kp.Value.IsOnDraw && kp.Value.Wrapper.GetCanvas.Equals(wrapper))
                    {
                        return kp.Value;
                    }
                }
                return null;
            }
        }
    }
}
