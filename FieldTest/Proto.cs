﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FieldTest
{
    class Proto
    {
        static private byte[] getCmd(string mode, int channel, UInt64 countPack, UInt64 interval)
        {
            string cmd = mode + " " + channel.ToString() + " " + countPack.ToString() + " " + interval.ToString()+"\r\n";
            return Encoding.UTF8.GetBytes(cmd);
        }

        static public byte[] GetServiceCmd()
        {
            return Encoding.UTF8.GetBytes("service\r\n");
        }

        static public byte[] GetStatCmd()
        {
            return Encoding.UTF8.GetBytes("stat\r\n");
        }

        static public byte[] GetRxCmd(int channel, UInt64 countPack, UInt64 interval)
        {
            return getCmd("rx", channel, countPack, interval);
        }

        static public byte[] GetTxCmd(int channel, UInt64 countPack, UInt64 interval)
        {
            return getCmd("tx", channel, countPack, interval);
        }

        static public byte[] GetStopCmd()
        {
            return Encoding.UTF8.GetBytes("stop\r\n");
        }
    }
}
