﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO.Ports;
using System.Threading;
using Newtonsoft.Json;
using System.Windows.Forms;
using System.IO;

namespace FieldTest
{
    class Driver
    {
        private const byte START_MARKER_1 = 0xaa;
        private const byte START_MARKER_2 = 0xbb;

        private const byte CMD_STAT = 0x0c;
        private const byte CMD_LOSS = 0x0d;
        private const byte CMD_RSSIVAL = 0x0e;
        private const byte CMD_LASTID = 0x0f;
        private const byte CMD_TIMEOUT = 0x10;
        private const byte CMD_PING = 0x11;
        private const byte CMD_FIRSTPACKET = 0x12;



        private const int C_TIMEOUT_SERIAL = 100;

        // settings class
        public class TestMode
        {
            public int _channel;
            public ulong _countPackets;
            public ulong _interval;
        }

        // enums
        private enum DRIVER_STATE
        {
            IDLE = 0,
            START,
            POLL,
            STOP,
            SERVICE_PEND
        }
        enum POLLMODE
        {
            PM_NOT,
            PM_ALL_STATISTICS_GET,
            PM_NEED_SERVICE_INFO,
            PM_ALL_SERVICEINFO_GET,
            PM_TIMEOUT_DEVICE_EXPIRED,
            PM_TIMEOUT_WAITING_EXPIRED,
            PM_TEST_IS_OVER,
            PM_NEED_STATISTICS
        }
        enum TREATMENT_FLAGS
        {
            TF_NONE,
            TF_NEWLINE,

        }
        enum RECEIVE_STATUS
        {
            RECEIVE_IN_PROGRESS,
            RECEIVE_NOT,
        }

        enum PACKET_PARSER
        {
            NONE,
            START_MARKER_1_DONE,
            START_MARKER_2_DONE,
            LEN_LOW_DONE,
            LEN_HIGH_DONE,
            ALL_DATA_DONE
        }
        PACKET_PARSER _packetParser = PACKET_PARSER.NONE;
        int curLenPacket;
        int needLenPacket;
        byte needLenPacket_Low;
        byte needLenPacket_High;
        List<byte> dataBuffer = new List<byte>();


        POLLMODE _pollMode = POLLMODE.PM_NOT;
        DRIVER_STATE _stateDriver = DRIVER_STATE.IDLE;
        TREATMENT_FLAGS _receiveFlags = TREATMENT_FLAGS.TF_NONE;
        RECEIVE_STATUS _receiveStatus = RECEIVE_STATUS.RECEIVE_NOT;

        // flags
        private volatile bool F_IS_FIRST_PACKET_RECEIVE = false;
        private volatile bool F_IS_RUN_DRIVER = false;
        private volatile int F_COUNT_STAT_PARAM = 0;
        private volatile bool F_IS_SERVICE_INFO_GET = false;
        private volatile bool F_IS_ALLOW_RX = false;

        // threads
        Thread workerThread;
        Thread receiveThread;

        // caller object (form)
        Form1 listener;

        // Serial Port
        SerialPort serial;
        byte[] rxBuffer = new byte[4000];
        int rxBufferPtr = 0;

        // settings object
        TestMode settings;

        // statistics
        Statistics stat;


        public static class PbarInfo
        {
            public static int maxValue;
            public static int curValue;
            public static int minValue = 0;
            public static int progress;
        }   
        Thread progressThread;
        
        
        DateTime timeBegin;
        DateTime timeFinish;

        public Driver(Form1 form)
        {
            // ставим флаг о том, что драйвер запустили, чтобы потоки запустились
            F_IS_RUN_DRIVER = true;

            this.listener = form;

            // запуск основного потока
            workerThread = new Thread(new ThreadStart(Run));
            workerThread.IsBackground = true;
            workerThread.Start();

            // запуск потока для Rx Serial
            receiveThread = new Thread(new ThreadStart(SerialRead));
            receiveThread.IsBackground = true;
            receiveThread.Start();

            _stateDriver = DRIVER_STATE.IDLE;
        }

        public void Start(string port, object sett, int timeout = 50)
        {
            if(serial != null)
            {
                try
                {
                    if (serial.IsOpen) serial.Close();
                }
                finally
                {
                    serial = null;
                }
            }

            bool isSerialOpen = false;
            int countAttempt = 0;

            // открываем ком порт
            if (serial == null)
            {
                serial = new SerialPort(port, 115200);
                while (countAttempt < 5 || !isSerialOpen)
                {
                    try
                    {
                        serial.Open();
                        isSerialOpen = true;
                    }
                    catch (Exception e)
                    {
                        //listener.UpdateTextBox(listener.tboxSerial, " Error open serial port");
                        //return;
                        countAttempt++;
                    }
                }
                if(!isSerialOpen)
                {
                    listener.UpdateTextBox(listener.tboxSerial, " Error open serial port");
                    return;
                }
            }

            stat = new Statistics(this.listener);
            settings = (TestMode)sett;
            stat.intervalDuty = (Int32)settings._interval;
            stat.sumPackets = (Int32)settings._countPackets;

            prepare();
            _pollMode = POLLMODE.PM_NOT;
            _stateDriver = DRIVER_STATE.START;

            Graphs.GraphController.Graphics[Graphs.GraphController.GRAPH_TYPE.RSSI_GOOD].Clear();
            Graphs.GraphController.Graphics[Graphs.GraphController.GRAPH_TYPE.RSSI_BAD].Clear();


            /*
            Graphs.Graph graph = Graphs.GraphController.Graphics[Graphs.GraphController.GRAPH_TYPE.RSSI_GOOD];
            graph.Fill(new Graphs.GraphParams()
            {
                countPoints = 1,
                xInterval = 1.0,
                yInterval = 1.0,
                needPointsX = new List<long>() { 0 },
                needPointsY = new List<long>() { 0 }
            });
            */


            //graph.DrawWithRescale();
        }

        private void prepare()
        {
            F_COUNT_STAT_PARAM = 0;
            F_IS_ALLOW_RX = false;
            F_IS_SERVICE_INFO_GET = false;

            timeBegin = DateTime.Now;
            listener.UpdateLabel(listener.lblTimeBegin, timeBegin.ToString("HH:mm:ss"));
            listener.UpdateLabel(listener.lblTimeFinish, "-");
            listener.UpdateLabel(listener.lblTimeEstimatedMinute, "-");
            listener.UpdateLabel(listener.lblTmeEstimatedSeconds, "-");
            listener.UpdateTextBox(listener.tboxSerial, "");
        }

        public Driver(Form1 listener, string port, object settings, int timeout = 50)
        {
            //if(_listener == null)
            //    _listener = listener;
            /*if(serial == null)
            {
                serial = new SerialPort(port, 115200);
                //serial.DataReceived += OnSerialReceive;
                try
                {
                    serial.Open();
                }
                catch(Exception e)
                {
                    listener.UpdateTextBox(_listener.tboxSerial, " Error open serial port");
                    return;
                }
            }*/
            //if (stat == null)
            //    stat = new Statistics(listener);
            //else stat.Clear();

            //_rxBuffer = new byte[1024];
            //_rxPointer = 0;
            //_settings = (TestMode)settings;
            //_timeoutPoll = timeout;

            

            //if(workerThread == null)
            //{
            //    workerThread = new Thread(new ThreadStart(Run));
            //    workerThread.IsBackground = true;
            //    workerThread.Start();
            //}
            
            //_isRun = true;
            //_state = DriverState.START;

            //_isRead = false;
            //_isStat = false;
            //_isTimeoutExpired = false;
            //_listener.UpdateProgress(_listener.pBarPackets, 0);

            //PbarInfo.maxValue = (int)_settings._countPackets;
            //PbarInfo.curValue = 0;


            //progressThread = new Thread(new ThreadStart(pBarThread));
            //progressThread.IsBackground = true;
            //progressThread.Start();

            //if (receiveThread == null)
            //{
            //    receiveThread = new Thread(new ThreadStart(SerialRead));
            //    receiveThread.IsBackground = true;
            //    receiveThread.Start();
            //}

            //timeBegin = DateTime.Now;
            //_listener.UpdateLabel(_listener.lblTimeBegin, timeBegin.ToString("HH:mm:ss"));
            //_listener.UpdateLabel(_listener.lblTimeFinish, "-");
            //_listener.UpdateLabel(_listener.lblTimeEstimatedMinute, "-");
            //_listener.UpdateLabel(_listener.lblTmeEstimatedSeconds, "-");
            //_listener.UpdateTextBox(_listener.tboxSerial, "");

        }

        ~Driver()
        {
            //Stop();
        }

        static public string[] GetSerialPorts()
        {
            return SerialPort.GetPortNames();
        }

        private void pBarThread()
        {
            while(true)
            {
                
                if(F_IS_FIRST_PACKET_RECEIVE)
                {
                    //_listener.UpdateProgress(_listener.pBarPackets, ++PbarInfo.curValue);
                }
                //if (_isEnd)
                //{
                    //_listener.UpdateProgress(_listener.pBarPackets, _listener.pBarPackets.Maximum);
                //    break;
                //}
                //Thread.Sleep((int)_settings._interval);
                Thread.Sleep(100);
            }
        }

        private void clearRxBuffer()
        {
            for (int i = 0; i < rxBuffer.Length; i++)
            {
                rxBuffer[i] = (byte)'0';
            }
            
            rxBufferPtr = 0;
            //_isLine = false;
        }

        private void finishTest()
        {
            timeFinish = DateTime.Now;
            listener.UpdateLabel(listener.lblTimeFinish, timeFinish.ToString("HH:mm:ss"));
            TimeSpan timeDiff = timeFinish - timeBegin;
            listener.UpdateLabel(listener.lblTimeEstimatedMinute, timeDiff.Minutes.ToString());
            listener.UpdateLabel(listener.lblTmeEstimatedSeconds, timeDiff.Seconds.ToString());
            stat.Fill(timeBegin, timeFinish);

            _stateDriver = DRIVER_STATE.IDLE;
            _pollMode = POLLMODE.PM_NOT;
            _receiveFlags = TREATMENT_FLAGS.TF_NONE;
            F_COUNT_STAT_PARAM = 0;
            F_IS_ALLOW_RX = false;
            F_IS_FIRST_PACKET_RECEIVE = false;
            F_IS_SERVICE_INFO_GET = false;

            // закрываем ком порт
            if(serial !=null && serial.IsOpen)
            {
                try
                {
                    serial.Close();
                    serial = null;
                }
                catch(Exception e)
                {
                    listener.AddToTextBox(listener.tboxSerial, "error close serial");
                    return;
                }
            }

            // очищаем статистику
            //if (stat != null)
            //{
            //    stat.Clear();
            //}
        }


        private void partingPacket()
        {
            
            int counter = 0;
            while (counter < rxBufferPtr && _packetParser != PACKET_PARSER.ALL_DATA_DONE)
            {
                switch (_packetParser)
                {
                    case PACKET_PARSER.NONE:
                        {
                            if (rxBuffer[counter] == START_MARKER_1)
                            {
                                _packetParser = PACKET_PARSER.START_MARKER_1_DONE;
                            }
                            break;
                        }
                    case PACKET_PARSER.START_MARKER_1_DONE:
                        {
                            if (rxBuffer[counter] == START_MARKER_2)
                            {
                                _packetParser = PACKET_PARSER.START_MARKER_2_DONE;
                            }
                            break;
                        }
                    case PACKET_PARSER.START_MARKER_2_DONE:
                        {
                            needLenPacket_Low = rxBuffer[counter];
                            _packetParser = PACKET_PARSER.LEN_LOW_DONE;
                            break;
                        }
                    case PACKET_PARSER.LEN_LOW_DONE:
                        {
                            needLenPacket_High = rxBuffer[counter];
                            byte[] b = new byte[2];
                            b[0] = needLenPacket_Low;
                            b[1] = needLenPacket_High;
                            needLenPacket = BitConverter.ToInt16(b, 0);
                            _packetParser = PACKET_PARSER.LEN_HIGH_DONE;
                            dataBuffer.Clear();
                            curLenPacket = 0;
                            break;
                        }
                    case PACKET_PARSER.LEN_HIGH_DONE:
                        {
                            dataBuffer.Add(rxBuffer[counter]);
                            curLenPacket++;
                            if (curLenPacket == needLenPacket)
                            {
                                _packetParser = PACKET_PARSER.ALL_DATA_DONE;
                            }
                            break;
                        }
                }
                counter++;
            }
        }

        private void TreatmentReceive()
        {
            while (rxBufferPtr != 0)
            {
                partingPacket();
                if (_packetParser == PACKET_PARSER.ALL_DATA_DONE)
                {
                    _packetParser = PACKET_PARSER.NONE;
                    byte[] b = new byte[curLenPacket];
                    Array.Copy(dataBuffer.ToArray(), 0, b, 0, curLenPacket);
                    ParsePacket(b);

                    int beginPos = 4 + needLenPacket;
                    int lastPos = rxBufferPtr;

                    // clear buffer
                    if (beginPos == lastPos) rxBufferPtr = 0;

                    else
                    {
                        for (var i = beginPos; i < rxBufferPtr; i++)
                        {
                            rxBuffer[i - beginPos] = rxBuffer[i];
                        }
                        rxBufferPtr = rxBufferPtr - beginPos;
                    }
                }
                else
                {
                    listener.AddToTextBox(listener.tboxSerial, "not full packet, break");
                    _packetParser = PACKET_PARSER.NONE;
                    break;
                }
            }

            
        }

        private void ParsePacket(byte[] b)
        {
            switch(b[0])
            {
                case CMD_PING:
                    {
                        listener.AddToTextBox(listener.tboxSerial, ".");
                        // - .
                        break;
                    }
                case CMD_FIRSTPACKET:
                    {
                        listener.AddToTextBox(listener.tboxSerial, "First packet Find\r\n");
                        // - !
                        break;
                    }
                case CMD_LASTID:
                    {
                        listener.AddToTextBox(listener.tboxSerial, "Last Id received\r\n");
                        _pollMode = POLLMODE.PM_TEST_IS_OVER;
                        // - last id received
                        break;
                    }
                case CMD_LOSS:
                    {
                        listener.AddToTextBox(listener.tboxSerial, "Loss: ");
                        List<long> arrList = new List<long>();
                        for (var i = 1; i < b.Length - 1; i += 8) 
                        {
                            byte[] arr = new byte[8];
                            for (var j = 0; j < 8; j++)
                            {
                                arr[j] = b[i + j];
                            }
                            UInt64 value = BitConverter.ToUInt64(arr, 0);
                            arrList.Add((long)value);
                            stat.lossIdsArray.Add((long)value);
                        }
                        foreach(var l in arrList)
                        {
                            listener.AddToTextBox(listener.tboxSerial, l.ToString()+",");
                        }
                        listener.AddToTextBox(listener.tboxSerial, "\r\n");

                        //stat.lossIdsArray.AddRange(arrList);
                        // - loss:fgf
                        break;
                    }
                case CMD_RSSIVAL:
                    {
                        double rssiVal = (b[1] / 2 - 130);
                        int isUseful = b[2];
                        listener.AddToTextBox(listener.tboxSerial, "rssivalue: " + rssiVal + ", isUseful: " + isUseful + "\r\n");

                        Graphs.Graph graph = Graphs.GraphController.Graphics[Graphs.GraphController.GRAPH_TYPE.RSSI_GOOD];
                        if (isUseful == 1)
                        {
                            if (graph.IsEmpty)
                            {
                                graph.Fill(new Graphs.GraphParams()
                                {
                                    countPoints = 1,
                                    xInterval = 1.0,
                                    yInterval = 1.0,
                                    needPointsX = new List<long>() { (long)graph.AxisX.Grid },
                                    needPointsY = new List<long>() { (long)rssiVal }
                                });
                            }
                            else
                            {
                                graph.AddYPoint(rssiVal);
                            }
                            graph.DrawWithRescale();
                        }

                        else if(isUseful == 0)
                        {
                            double x = graph.AxisX.Max;
                            Graphs.Graph graphBad = Graphs.GraphController.Graphics[Graphs.GraphController.GRAPH_TYPE.RSSI_BAD];
                            if (graphBad.IsEmpty)
                            {
                                graphBad.Fill(new Graphs.GraphParams()
                                {
                                    countPoints = 1,
                                    xInterval = 1.0,
                                    yInterval = 1.0,
                                    needPointsX = new List<long>() { (long)x },
                                    needPointsY = new List<long>() { (long)rssiVal }
                                });
                            }
                            else
                            {
                                graphBad.AddPoint(x, rssiVal);
                            }
                            graphBad.Draw();
                        }

                        //GraphController.RssiGraph.OnlineGraph(rssiVal);
                        // - rsval:
                        break;
                    }
                case CMD_STAT:
                    {
                        listener.AddToTextBox(listener.tboxSerial, "Stat get\r\n");
                        byte[] recPackarr = new byte[8];
                        Array.Copy(b, 1, recPackarr, 0, 8);
                        UInt64 receivePackets = BitConverter.ToUInt64(recPackarr, 0);
                        stat.receivedPackets = (int)receivePackets;
                        F_COUNT_STAT_PARAM++;

                        Array.Copy(b, 9, recPackarr, 0, 8);
                        UInt64 brokenPackets = BitConverter.ToUInt64(recPackarr, 0);
                        stat.brokenPackets = (int)brokenPackets;
                        F_COUNT_STAT_PARAM++;

                        Array.Copy(b, 17, recPackarr, 0, 4);
                        Int32 Percent = BitConverter.ToInt32(recPackarr, 0);
                        stat.percentDelivery = (int)Percent;
                        F_COUNT_STAT_PARAM++;

                        Array.Copy(b, 21, recPackarr, 0, 4);
                        Int32 Rssi = BitConverter.ToInt32(recPackarr, 0);
                        stat.rssiUseful = (int)Rssi;
                        F_COUNT_STAT_PARAM++;

                        // - stat
                        break;
                    }
                case CMD_TIMEOUT:
                    {
                        listener.AddToTextBox(listener.tboxSerial, "timeout expired\r\n");
                        _pollMode = POLLMODE.PM_TEST_IS_OVER;
                        // - timeout expired
                        break;
                    }
            }
        }

        private void OnSerialReceive()
        {
            byte[] response = new byte[4000];
            int result = 0;
            try
            {
                result = SerialReadBytes(ref response);
            }
            catch(Exception e)
            {
                listener.AddToTextBox(listener.tboxSerial, "error read serial: "+e);
                F_IS_ALLOW_RX = false;
                return;
            }
            
            if(result < 0)
            {
                // exception Read
            }
            else if(result == 0)
            {
                // not bytes to read
            }
            else
            {
                F_IS_ALLOW_RX = false;
                // read result bytes
                Array.Copy(response, 0, rxBuffer, rxBufferPtr, result);

                
                string s = BitConverter.ToString(response, 0, result);
                listener.AddToTextBox(listener.tboxSerial, s);

                rxBufferPtr += result;

                TreatmentReceive();

                //clearRxBuffer();



                /*
                string s = Encoding.UTF8.GetString(rxBuffer, 0, rxBufferPtr);
                listener.AddToTextBox(listener.tboxSerial, s);
                if (TreatmentReceiveBuffer(s)) 
                {
                    clearRxBuffer();
                }
                else
                {
                    int a = 5;
                }

                */

                F_IS_ALLOW_RX = true;
            }
        }

        private bool TreatmentReceiveBuffer(string s)
        {
            if(s.Contains("\r"))
            {
                _receiveFlags = TREATMENT_FLAGS.TF_NEWLINE;
            }
            if (s.Contains("."))
            {
                //if (!F_IS_FIRST_PACKET_RECEIVE)
                //{
                //    F_IS_FIRST_PACKET_RECEIVE = true;
                    //_listener.UpdateProgress(_listener.pBarPackets, ++PbarInfo.curValue);
                //}
            }
            if (_receiveFlags == TREATMENT_FLAGS.TF_NEWLINE)
            {
                _receiveFlags = TREATMENT_FLAGS.TF_NONE;
                string newStr = s.Replace("\r\n", "(");
                string[] ss = newStr.Split('(');

                foreach (string s1 in ss)
                {
                    if (s1.Contains("mcu"))
                    {
                    }
                    if(s1.Contains("loss"))
                    {
                        int startIndex = s1.IndexOf(":") + 2;
                        string loss = s1.Substring(startIndex);
                        stat.lossIds += loss;
                    }
                    if (s1.Contains("last id received"))
                    {
                        stat.statusFinish = "last Id received";
                        _pollMode = POLLMODE.PM_TEST_IS_OVER;
                    }
                    if (s1.Contains("timeout"))
                    {
                        stat.statusFinish = "timeout expired";
                        _pollMode = POLLMODE.PM_TEST_IS_OVER;
                        //_pollMode = POLLMODE.PM_TIMEOUT_DEVICE_EXPIRED;
                        //_isTimeoutExpired = true;
                    }
                    if(s1.Contains("rsval"))
                    {
                        int startIndex = s1.IndexOf(":") + 2;
                        string rssiStr = s1.Substring(startIndex);
                        double rssi = Convert.ToDouble(rssiStr);
                        //GraphController.RssiGraph.OnlineGraph(rssi);
                    }

                    if (s1.Contains("firstPacketId"))
                    {
                        int startIndex = s1.IndexOf(":") + 2;
                        string firstId = s1.Substring(startIndex);
                        stat.firstId = Convert.ToInt32(firstId);

                    }
                    if (s1.Contains("listLossIds"))
                    {
                        int startIndex = s1.IndexOf(":") + 2;
                        string lossIds = s1.Substring(startIndex);
                        stat.lossIds = lossIds;
                        _pollMode = POLLMODE.PM_ALL_SERVICEINFO_GET;
                    }
                    if (s1.Contains("ReceivedPackets"))
                    {
                        int startIndex = s1.IndexOf(":") + 2;
                        string recPackets = s1.Substring(startIndex);
                        stat.receivedPackets = Convert.ToInt32(recPackets);
                        F_COUNT_STAT_PARAM++;
                    }
                    if (s1.Contains("BrokenPackets"))
                    {
                        int startIndex = s1.IndexOf(":") + 2;
                        string brokPacket = s1.Substring(startIndex);
                        stat.brokenPackets = Convert.ToInt32(brokPacket);
                        F_COUNT_STAT_PARAM++;
                    }
                    if (s1.Contains("percentDelivery"))
                    {
                        int startIndex = s1.IndexOf(":") + 2;
                        int stopIndex = s1.IndexOf("%");
                        string percent = s1.Substring(startIndex, stopIndex - startIndex);
                        stat.percentDelivery = Convert.ToInt32(percent);
                        F_COUNT_STAT_PARAM++;
                    }
                    if (s1.Contains("rssiUseful"))
                    {
                        int startIndex = s1.IndexOf(":") + 2;
                        string rssi = s1.Substring(startIndex);
                        stat.rssiUseful = Convert.ToInt32(rssi);
                        F_COUNT_STAT_PARAM++;
                    }
                }
                return true;
            }
            return false;
        }

        void SerialRead()
        {
            // если работает драйвер
            while (F_IS_RUN_DRIVER)
            {
                // если разрешен прием
                if (F_IS_ALLOW_RX && 
                    serial != null && 
                    serial.IsOpen)
                {
                    OnSerialReceive();
                }
                Thread.Sleep(100);
            }
        }

        private void Run()
        {
            while(F_IS_RUN_DRIVER)
            {
                CheckState();
                //SerialRead();
                Thread.Sleep(50);
            }
        }

        private int SerialWrite(byte[] req)
        {
            serial.DiscardInBuffer();
            Stream serialStream = serial.BaseStream;
            int cntr = 0;
            bool isExceptWrite = false;
            int timeout = C_TIMEOUT_SERIAL;
            bool isWr = false;
            //if (OsName == "linux" && !File.Exists(serial.PortName))
            //{
                //abortConnection();
            //    throw new Exception("serial port not connected");
            //}
            serialStream.BeginWrite(req, 0, req.Length, new AsyncCallback((IAsyncResult result) => {
                try
                {
                    Stream stream = (Stream)result.AsyncState;
                    stream.EndWrite(result);
                    isWr = true;
                }
                catch
                {
                    isExceptWrite = true;
                }
            }), serialStream);
            while (!isWr && cntr < timeout)
            {
                cntr++;
                Thread.Sleep(1);
            }
            if (!isWr || isExceptWrite)
            {
                //abortConnection ();
                //throw new Exception("error serial write");
                return -1;
            }
            return req.Length;
        }

        private int SerialReadBytes(ref byte[] b)
        {
            int cntr = 0;
            int timeout = C_TIMEOUT_SERIAL;
            bool isRd = false;
            bool isExceptRead = false;
            byte[] resp = new byte[4000];
            Stream serialStream = serial.BaseStream;
            serialStream.ReadTimeout = 100;
            int countRead = serial.BytesToRead;
            serialStream.BeginRead(resp, 0, countRead, new AsyncCallback((IAsyncResult result) => {
                try
                {
                    Stream stream = (Stream)result.AsyncState;
                    countRead = stream.EndRead(result);
                    isRd = true;
                }
                catch(Exception e)
                {
                    //throw e;
                    isExceptRead = true;
                }
            }), serialStream);
            while (!isRd && cntr < timeout)
            {
                cntr++;
                Thread.Sleep(1);
            }
            if(isExceptRead || !isRd)
            {
                return -1;
            }
            //if (!isRd)
            //{
                //abortConnection ();
                //throw new Exception("error serial read");
            //    return false;
            //}
            b = resp;
            //return resp[0];
            return countRead;
        }

        private void CheckState()
        {
            switch(_stateDriver)
            {
                case DRIVER_STATE.IDLE:
                    {

                        break;
                    }
                case DRIVER_STATE.POLL:
                    {
                        switch(_pollMode)
                        {
                            case POLLMODE.PM_NOT:
                                {
                                    // waiting for statistics cmd from device
                                    break;
                                }
                            case POLLMODE.PM_TEST_IS_OVER:
                                {
                                    F_IS_ALLOW_RX = false;
                                    byte[] cmd = Proto.GetStatCmd();
                                    SerialWrite(cmd);
                                    F_IS_ALLOW_RX = true;
                                    _pollMode = POLLMODE.PM_NEED_STATISTICS;
                                    break;
                                }
                            case POLLMODE.PM_NEED_STATISTICS:
                                {
                                    // waiting for statistics cmd from device
                                    if(F_COUNT_STAT_PARAM == 4)
                                    {
                                        _pollMode = POLLMODE.PM_ALL_STATISTICS_GET;
                                        F_COUNT_STAT_PARAM = 0;
                                    }
                                    break;
                                }
                            case POLLMODE.PM_ALL_STATISTICS_GET:
                                {
                                    // statistics info is getting
                                    // тут надо отправить запрос в Serial port

                                    //F_IS_ALLOW_RX = false;
                                    //Thread.Sleep(500);
                                    //byte[] cmd = Proto.GetServiceCmd();
                                    //SerialWrite(cmd);
                                    //F_IS_ALLOW_RX = true;

                                    //_pollMode = POLLMODE.PM_NEED_SERVICE_INFO;

                                    _pollMode = POLLMODE.PM_ALL_SERVICEINFO_GET;
                                    F_IS_SERVICE_INFO_GET = true;

                                    break;
                                }
                            case POLLMODE.PM_NEED_SERVICE_INFO:
                                {
                                    // waiting for service info from device

                                    if (F_IS_SERVICE_INFO_GET)
                                    {
                                        _pollMode = POLLMODE.PM_ALL_SERVICEINFO_GET;
                                        F_IS_SERVICE_INFO_GET = false;
                                    }
                                    break;
                                }
                            case POLLMODE.PM_ALL_SERVICEINFO_GET:
                                {
                                    // тут высе заканчиваем
                                    // выходим из POLL-mode
                                    _stateDriver = DRIVER_STATE.IDLE;
                                    finishTest();
                                    break;
                                }
                            case POLLMODE.PM_TIMEOUT_DEVICE_EXPIRED:
                                {
                                    _stateDriver = DRIVER_STATE.IDLE;
                                    finishTest();
                                    break;
                                }
                            case POLLMODE.PM_TIMEOUT_WAITING_EXPIRED:
                                {
                                    break;
                                }
                            default: break;
                        }
                        break;
                    }
                case DRIVER_STATE.START:
                    {
                        // отправляем команду на старт девайсу
                        F_IS_ALLOW_RX = false;
                        byte[] cmd = Proto.GetRxCmd(settings._channel, settings._countPackets, settings._interval);
                        SerialWrite(cmd);
                        // переходим в режим сканирования
                        _stateDriver = DRIVER_STATE.POLL;
                        // разрешаем прием байтов из Serial порта
                        F_IS_ALLOW_RX = true;
                        break;
                    }
                case DRIVER_STATE.STOP:
                    {
                       
                        break;
                    }
            }
        }

    }
}
