﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms.DataVisualization.Charting;

namespace FieldTest
{

    public interface IGraph
    {
        void Clear();
        void Update(double[,] data);
        void SetMaxX(double value);
        void SetMinX(double value);
        void SetMinY(double value);
        void SetMaxY(double value);
        void SetGridInterval(double value);
    }

    class GraphWrapper : IGraph
    {
        delegate void UpdateChartDoubleValue(Chart chart, double value);
        delegate void UpdateChartVoid(Chart chart);
        delegate void UpdateChartData(Chart chart, double[,] data);

        private Chart _chart;
        public GraphWrapper(Chart chart)
        {
            _chart = chart;
        }

        public void Clear()
        {
            if (_chart == null) return;

            if (_chart.InvokeRequired)
            {
                _chart.Invoke(new UpdateChartVoid((c) =>
                {
                    c.Series[0].Points.Clear();
                }), new object[] { _chart });

                return;
            }
            _chart.Series[0].Points.Clear();
        }

        public void SetMaxX(double value)
        {
            if (_chart == null) return;
            if (_chart.InvokeRequired)
            {
                _chart.Invoke(new UpdateChartDoubleValue((c,d) =>
                {
                    c.ChartAreas[0].AxisX.Maximum = d;
                }), new object[] { _chart, value });

                return;
            }
            _chart.ChartAreas[0].AxisX.Maximum = value;
        }

        public void SetMinX(double value)
        {
            if (_chart == null) return;
            if (_chart.InvokeRequired)
            {
                _chart.Invoke(new UpdateChartDoubleValue((c, d) =>
                {
                    c.ChartAreas[0].AxisX.Minimum = d;
                }), new object[] { _chart, value });

                return;
            }
            _chart.ChartAreas[0].AxisX.Minimum = value;
        }

        public void SetMaxY(double value)
        {
            if (_chart == null) return;
            if (_chart.InvokeRequired)
            {
                _chart.Invoke(new UpdateChartDoubleValue((c, d) =>
                {
                    c.ChartAreas[0].AxisY.Maximum = d;
                }), new object[] { _chart, value });

                return;
            }
            _chart.ChartAreas[0].AxisY.Maximum = value;
        }

        public void SetMinY(double value)
        {
            if (_chart == null) return;
            if (_chart.InvokeRequired)
            {
                _chart.Invoke(new UpdateChartDoubleValue((c, d) =>
                {
                    c.ChartAreas[0].AxisY.Minimum = d;
                }), new object[] { _chart, value });

                return;
            }
            _chart.ChartAreas[0].AxisY.Minimum = value;
        }

        public void Update(double[,] data)
        {
            if (_chart == null) return;
            if (_chart.InvokeRequired)
            {
                _chart.Invoke(new UpdateChartData((c, d) =>
                {
                    for (var i = 0; i < d.Length / 2; i++)
                    {
                        c.Series[0].Points.AddXY(d[i, 0], d[i, 1]);
                    }
                }), new object[] { _chart, data });

                return;
            }
            for (var i = 0; i < data.Length / 2; i++)
            {
                _chart.Series[0].Points.AddXY(data[i, 0], data[i, 1]);
            }
        }

        public void SetGridInterval(double value)
        {
            if (_chart == null) return;
            if (_chart.InvokeRequired)
            {
                _chart.Invoke(new UpdateChartDoubleValue((c, d) =>
                {
                    c.ChartAreas[0].AxisX.MajorGrid.Interval = d;
                }), new object[] { _chart, value });

                return;
            }
            _chart.ChartAreas[0].AxisX.MajorGrid.Interval = value;
        }
    }

    
    class Point
    {
        double _x;
        double _y;
        public double X { get { return _x; } }
        public double Y { get { return _y; } }
        public Point(double x, double y)
        {
            _x = x;
            _y = y;
        }
    }


    public class GraphAbs
    {
        List<Point> data;
        bool isInited;
        double maximum;
        double minimum;
        double grid;
        bool isOriginal = true;

        double yMinimum;
        double yMaximum;
        double yGrid;
        double yCurMinimum;
        double yCurMaximum;

        double curMaximum;
        double curMinimum;

        public GraphAbs()
        {
            data = new List<Point>();
        }

        public bool IsOriginal
        {
            get { return this.isOriginal; }
        }

        public double Min
        {
            get { return this.minimum; }
            set { this.minimum = value; }
        }
        public double Max
        {
            get { return this.maximum; }
            set { this.maximum = value; }
        }
        public double CurMaximum
        {
            get { return this.curMaximum; }
            set { this.curMaximum = value; }
        }
        public double CurMinumum
        {
            get { return this.curMinimum; }
            set { this.curMinimum = value; }
        }

        public double YMin
        {
            get
            {
                return this.data.Min(t => t.Y);
            }
        }
        public double YMax
        {
            get
            {
                return this.data.Max(t => t.Y);
            }
        }
        public double YCurMinimum
        {
            get { return this.yCurMinimum; }
            set { this.yCurMinimum = value; }
        }
        public double YCurMaximum
        {
            get { return this.yCurMaximum; }
            set { this.yCurMaximum = value; }
        }
        public double YGrid
        {
            get { return this.yGrid; }
            set { this.yGrid = value; }
        }

        public double Grid
        {
            get { return this.grid; }
        }
        public bool IsInited
        {
            get { return this.isInited; }
        }

        public double[,] ToArray()
        {
            double[,] d = new double[data.Count, 2];
            for (var i = 0; i < data.Count; i++)
            {
                d[i, 0] = data[i].X;
                d[i, 1] = data[i].Y;
            }
            return d;
        }

        public void Inverse()
        {
            if (data == null || data.Count == 0) return;

            List<Point> inverseData = new List<Point>(data.Count);
            for (var i = 0; i < data.Count; i++)
            {
                Point nPoint = new Point(data[i].X, data[(data.Count - 1) - i].Y);
                inverseData.Add(nPoint);
            }
            data = inverseData;

            this.isOriginal = !this.isOriginal;

        }

        public void AddOnlinePoint(double y)
        {
            if (this.data == null) return;

            double lastX = this.data[this.data.Count - 1].X;
            lastX += 1.0;


            Point p = new Point(lastX, y);

            this.data.Add(p);

            var interval = 0.0;

            if (this.data.Count > 1) interval = this.data[this.data.Count - 1].X - this.data[this.data.Count - 2].X;
            else if (this.data.Count == 1) interval = this.data[this.data.Count - 1].X;
            else return;

            this.Fill(interval);
        }

        /*
         * interval in milliseconds
         * grid in seconds, so divider coeff is 1000.0; int_sec = interval / 1000.0
         */
        public void Fill(int countPoints, double interval, List<long> lossPoint)
        {
            if (this.data == null) return;

            this.data.Clear();
            for (var i = 1; i <= countPoints; i++)
            {
                // точка x
                double x = Math.Round((double)i * interval, 1);

                // точка y
                bool isFind = lossPoint.Contains(i);
                double y = (isFind) ? 1 : 0;

                Point p = new Point(x, y);

                // график во времени
                this.data.Add(p);
            }

            this.Fill(interval);
        }

        public void Fill(GraphAbs graph, double xCoeff)
        {
            this.data.Clear();
            double curGrid = xCoeff * graph.Grid;
            foreach(var p in graph.data)
            {
                Point pp = new Point(Math.Round(p.X * curGrid / graph.Grid, 2), p.Y);
                this.data.Add(pp);
            }
            this.Fill(curGrid);
        }

        public void FillOnlineGraph()
        {
            this.data.Clear();
            double curGrid = 1.0;
            this.yCurMaximum = 0.0;
            this.YCurMinimum = -110.0;
            Point p = new Point(0, 0);
            this.data.Add(p);
            this.Fill(curGrid);
        }

        private void Fill(double interval)
        {
            this.grid = Math.Round(interval,2);
            this.minimum = this.curMinimum = this.data.Min(t => t.X);
            this.maximum = this.curMaximum = this.data.Max(t => t.X);

            this.YGrid = 1.0;

            this.isInited = true;
        }

        public void Clear()
        {
            this.isInited = false;
            this.data.Clear();
        }
    }
}
