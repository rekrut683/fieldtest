﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms.DataVisualization.Charting;

namespace FieldTest
{
    namespace Graphs
    {
        public class SeriesGraph
        {
            string name;
            object wrapper;
            public string Name
            {
                get { return this.name; }
            }
            public object Wrapper
            {
                get { return this.wrapper; }
                set { this.wrapper = value; }
            }
            public SeriesGraph(string name)
            {
                this.name = name;
            }
        }
        public class AxisGraph : ICloneable
        {
            private double _min;
            private double _max;
            private double _grid;

            public AxisGraph()
            {
                this._min = 0;
                this._max = 0.1;
            }

            public double Min
            {
                get { return this._min; }
                set
                {
                    this._min = Math.Round(value, 2);
                }
            }
            public double Max
            {
                get { return this._max; }
                set
                {
                    this._max = Math.Round(value, 2);
                }
            }
            public double Grid
            {
                get { return this._grid; }
                set
                {
                    this._grid = Math.Round(value, 2);
                }
            }
            public object Clone()
            {
                return this.MemberwiseClone();
            }
        }
        public class Point
        {
            double x;
            double y;
            public Point(double _x, double _y)
            {
                this.x = _x;
                this.y = _y;
            }
            public double X
            {
                get { return this.x; }
            }
            public double Y
            {
                get { return this.y; }
            }
        }
        public class GraphParams
        {
            public int countPoints;
            public double xInterval;
            public double yInterval;
            public List<long> needPointsX;
            public List<long> needPointsY;
        }

        public class Graph : ICloneable
        {
            protected SeriesWrapper wrapper;

            protected AxisGraph axisX;
            protected AxisGraph axisY;

            protected List<Point> points;

            public AxisGraph AxisX
            {
                get { return this.axisX; }
            }
            public SeriesWrapper Wrapper
            {
                get { return this.wrapper; }
            }
            public AxisGraph AxisY
            {
                get { return this.axisY; }
            }
            public bool IsEmpty
            {
                get { return !Convert.ToBoolean(this.points.Count); }
            }
            public object[,] ToArray
            {
                get
                {
                    object[,] d = new object[points.Count, 2];
                    for (var i = 0; i < points.Count; i++)
                    {
                        d[i, 0] = points[i].X;
                        d[i, 1] = points[i].Y;
                    }
                    return d;
                }
            }
            public bool IsOnDraw
            {
                get { return this.GetHashCode() == this.wrapper.GetCanvas.CurrentGraph; }
            }
            public void SetMax(AxisGraph axis, double value)
            {
                double refValue = (axis.Equals(this.axisX) == true) ? this.points.Max(x => x.X) : this.points.Max(x => x.Y);
                if (value >= refValue) value = refValue;
                if (value <= axis.Min) value = axis.Min + axis.Grid;
                axis.Max = value;
                wrapper.GetCanvas.SetMax(wrapper.GetCanvas.AxisX, axisX.Max);
                wrapper.GetCanvas.SetMax(wrapper.GetCanvas.AxisY, axisY.Max);
            }

            public void SetMin(AxisGraph axis, double value)
            {
                double refValue = (axis.Equals(this.axisX) == true) ? this.points.Min(x => x.X) : this.points.Min(x => x.Y);
                if (value <= refValue) value = refValue;
                if (value >= axis.Max) value = axis.Max - axis.Grid;
                axis.Min = value;
                wrapper.GetCanvas.SetMin(wrapper.GetCanvas.AxisX, axisX.Min);
                wrapper.GetCanvas.SetMin(wrapper.GetCanvas.AxisY, axisY.Min);
            }

            protected void recalcBorder()
            {
                if (points.Count == 0) return;
                axisX.Min = points.Min(x => x.X);
                axisX.Max = points.Max(x => x.X);
                if (axisX.Min == axisX.Max) axisX.Max = axisX.Min + axisX.Grid;

                axisY.Min = points.Min(x => x.Y);
                axisY.Max = points.Max(x => x.Y);
                if (axisY.Min == axisY.Max) axisY.Max = axisY.Min + axisY.Grid;
            }

            public Graph(Graphs.SeriesWrapper wrapper)
            {
                this.wrapper = wrapper;
                //wrapper = new GraphWrapper(this.view);
                points = new List<Point>();
                //collections = new List<CollectPoints>();
                axisX = new AxisGraph();
                axisY = new AxisGraph();
            }

            private void RescaleAxis()
            {
                // set X axis
                wrapper.GetCanvas.SetGridInterval(wrapper.GetCanvas.AxisX, axisX.Grid);
                wrapper.GetCanvas.SetMin(wrapper.GetCanvas.AxisX, axisX.Min);
                wrapper.GetCanvas.SetMax(wrapper.GetCanvas.AxisX, axisX.Max);

                // set Y axis
                wrapper.GetCanvas.SetGridInterval(wrapper.GetCanvas.AxisY, axisY.Grid);
                wrapper.GetCanvas.SetMin(wrapper.GetCanvas.AxisY, axisY.Min);
                wrapper.GetCanvas.SetMax(wrapper.GetCanvas.AxisY, axisY.Max);
            }

            public virtual void DrawWithRescale()
            {
                RescaleAxis();
                Draw();
            }

            public virtual void Draw()
            {
                // clear
                wrapper.Clear();

                // update data
                wrapper.Update(ToArray);

                wrapper.GetCanvas.CurrentGraph = this.GetHashCode();
            }

            public virtual void Clear()
            {
                this.points.Clear();
                //clear
                wrapper.Clear();
            }

            public virtual void Fill(GraphParams _params)
            {
                bool isX = false, isY = false;

                // точки для X
                if (_params.needPointsX != null && _params.needPointsX.Count != 0) isX = true;
                // точки для Y
                if (_params.needPointsY != null && _params.needPointsY.Count != 0) isY = true;
                // точки X и Y
                if (_params.needPointsX != null && _params.needPointsX.Count != 0 &&
                    _params.needPointsY != null && _params.needPointsY.Count != 0 &&
                    _params.countPoints == _params.needPointsY.Count && 
                    _params.countPoints == _params.needPointsX.Count)
                {
                    isX = true;
                    isY = true;
                }

                points.Clear();

                // только строим по точкам X
                if (isX && !isY)
                {
                    for (var i = 1; i <= _params.countPoints; i++)
                    {
                        // точка x
                        double x = Math.Round((double)i * _params.xInterval, 1);

                        // точка y
                        bool isFind = _params.needPointsX.Contains(i);
                        double y = (isFind) ? 1 : 0;

                        Point p = new Point(x, y);
                        points.Add(p);
                    }
                    
                }
                // по таблице X и Y
                else if(isX && isY)
                {
                    for (var i = 0; i < _params.countPoints; i++)
                    {
                        Point p = new Point(_params.needPointsX[i], _params.needPointsY[i]);
                        points.Add(p);
                    }
                }
                // ставим шкалу и границы графика
                axisX.Grid = _params.xInterval;
                axisY.Grid = _params.yInterval;
                recalcBorder();
            }

            public virtual void RescaleX(double coeff)
            {
                List<Point> newPoints = new List<Point>();
                foreach (var p in points)
                {
                    double newX = Math.Round(p.X * coeff, 2);
                    Point newP = new Point(newX, p.Y);
                    newPoints.Add(newP);
                }
                points = newPoints;
                axisX.Grid = Math.Round(axisX.Grid * coeff, 2);
                recalcBorder();
            }

            public virtual void RescaleY(double coeff)
            {

            }

            public virtual void InverseX()
            {
                if (points == null || points.Count == 0) return;

                List<Point> inverseData = new List<Point>();
                for (var i = 0; i < points.Count; i++)
                {
                    Point nPoint = new Point(points[i].X, points[(points.Count - 1) - i].Y);
                    inverseData.Add(nPoint);
                }
                points = inverseData;
            }

            public virtual void AddPoint(double x, double y)
            {
                Point p = new Point(x, y);
                points.Add(p);
                //recalcBorder();

                //points.Add(new Point(Math.Round(x, 2), Math.Round(y, 2)));
                //recalcBorder();
            }

            public void AddYPoint(double y)
            {
                double x = this.points.Max(val => val.X);
                x += this.axisX.Grid;
                Point p = new Point(x, y);
                points.Add(p);
                recalcBorder();
            }

            public void AddXPoint(double x)
            {

            }

            public object Clone()
            {
                Graphs.Graph gr = new Graphs.Graph(this.wrapper);
                gr.points = new List<Point>(this.points);
                gr.wrapper = this.wrapper;
                gr.axisX = (AxisGraph)this.axisX.Clone();
                gr.axisY = (AxisGraph)this.axisY.Clone();
                return gr;
            }
        }

    }
}
